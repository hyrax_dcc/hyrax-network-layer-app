#!/usr/bin/python3.5
import sys
import time
from datetime import datetime
import os
import random
import math
from hyrax_adb.adb import Adb
from hyrax_adb.log import Log
from hyrax_adb.command import Command
from hyrax_adb.command import WifiKey as WK
from consts import Consts as C

ALGS_MAP = {
    'flood': C.ROUTING_FLOOD,
    'flood_ctl': C.ROUTING_FLOOD_CONTROL,
    'bth_star': C.FORMATION_BLUETOOTH_STAR,
    'bth_mesh_rand': C.FORMATION_BLUETOOTH_MESH_RANDOM,
    'wifi_mesh_rand': C.FORMATION_WIFI_MESH_RANDOM,
    'wifi_p2p_star': C.FORMATION_WIFI_DIRECT_STAR,
    'wifi_p2p_leg_star': C.FORMATION_WIFI_DIRECT_LEGACY_STAR,
    'wifi_p2p_leg_star_2': C.FORMATION_WIFI_DIRECT_LEGACY_STAR_2,
    'wifi_hr_mix': 'wifi_hr_mix',
}


class Cmd(Command):

    def __init__(self):
        super().__init__("Adb command line options")

    # override
    def cmd_args(self, parser):
        # run network command
        route_parser = parser.add_parser(
            'route', help="Run a network routing expriment")
        route_parser.add_argument('-n', '--number', required=True, type=int,
                                  help="Define the number of devices to run the experiment")
        route_parser.add_argument('-g', '--group', type=int, help="Group size. To use \
                                with formation 'wifi_hr_mix'", default=0)
        route_parser.add_argument('-r', '--routing', required=True,
                                  choices=("flood", "flood_ctl"),
                                  help="Select the network routing algorithm")
        route_parser.add_argument('-f', '--formation', required=True,
                                  nargs='+',
                                  choices=("bth_star", "bth_mesh_rand",
                                           "wifi_mesh_rand", "wifi_p2p_star",
                                           "wifi_p2p_leg_star", "wifi_hr_mix"),
                                  help="Select the one or more network formation algorithm")
        route_parser.add_argument('--pause', help="Pauses the peers formation, before run packets",
                                  action="store_true")
        route_parser.add_argument('-d', '--delay', type=int, default=0,
                                  help="Defines a delay between every device network formation startup")
        route_parser.add_argument('-p', '--port', default='0',
                                  help="Define a port for the network viewer")
        route_parser.add_argument('-s', '--shuffle',  action="store_true",
                                  help="Shuffles the list of android devices")
        route_parser.set_defaults(cmd='route')

        top_parser = parser.add_parser('top',
                                       help="Run network formation topology")
        top_parser.add_argument('-n', '--number', required=True, type=int,
                                help="Define the number of devices to run the experiment")
        top_parser.add_argument('-g', '--group', type=int,
                                help="Group size.", default=0)
        top_parser.add_argument('-f', '--formation', required=True,
                                nargs='+',
                                choices=("bth_star", "bth_mesh_rand",
                                         "wifi_mesh_rand", "wifi_p2p_star",
                                         "wifi_p2p_leg_star", "wifi_p2p_leg_star_2", "wifi_hr_mix"),
                                help="Select the one or more network formation algorithm")
        top_parser.add_argument('-d', '--delay', type=int, default=0,
                                help="Defines a delay between every device network formation startup")
        top_parser.add_argument('-p', '--port', default='0',
                                help="Define a port for the network viewer")
        top_parser.add_argument('-r', '--repeat', type=int,
                                help="Number of repetitions")
        top_parser.set_defaults(cmd='top')

    # override
    def exec_command(self, options):
        formation = []
        for f in options.formation:
            if f not in ALGS_MAP:
                raise Exception("Undefined formation algorithm " + f)
            formation.append(ALGS_MAP[f])

        if options.cmd == "route":
            if options.routing not in ALGS_MAP:
                raise Exception(
                    "Undefined routing algorithm " + options.routing)
            routing = ALGS_MAP[options.routing]
            self.perform_route(options.number, options.group, routing, formation,
                               options.pause, options.port, options.delay, options.shuffle)
        elif options.cmd == "top":
            routing = ALGS_MAP['flood']
            self.perform_form_top(options.number, options.group, routing, formation,
                                  options.port, options.delay, options.repeat)
        else:
            raise Exception("Undefined command " + options.cmd)

    # override
    def get_wifi_key(self, wifi_enum_key):
        switcher = {
            WK.INT_ON: C.WIFI_HW_ENABLE,
            WK.INT_OFF: C.WIFI_HW_DISABLE,
            WK.INT_CONN_ON: C.WIFI_NET_CONNECT,
            WK.INT_CONN_OFF: C.WIFI_NET_DISCONNECT,
            WK.ARG_SSID: C.ARG_SSID,
            WK.ARG_PASS: C.ARG_PASS,
            WK.ARG_REMOVE: C.ARG_TO_REMOVE
        }
        return switcher[wifi_enum_key]

    def perform_form_top(self, number, group_size, routing, formation_list,
                         listen_port, delay, repeat):
        devices = Adb.get_devices()
        if len(devices) < number:
            raise Exception("Not enough connected devices")
        devices = devices[:number]
        Log.debug(devices)
        log_filename = "network.log"
        i = 4
        form_name = ""
        for form in formation_list:
            form_name += ("" if form_name == "" else "+") + form
        folder = C.LOGS_FOLDER + "%s/%s/%d/" % ("FORMATION", form_name, number)
        # creates logs folder of not existsd
        os.makedirs(folder, 0o777, True)

        while (i < repeat):
            Log.debug("Running experiment number %d" % (i + 1))
            # start android app
            Adb.start_app(devices, C.ACTIVITY)
            time.sleep(1)
            # sends the start network intent
            Adb.send_intent(devices, C.NETWORK_START, [
                (C.ARG_TYPE, routing),
                (C.ARG_LOG_FILE_NAME, log_filename),
                (C.ARG_LISTEN_PORT, listen_port)
            ])
            time.sleep(2)

            # connect to wifi
            if C.FORMATION_WIFI_DIRECT_LEGACY_STAR_2 in formation_list:
                print("Connection to wifi...")
                self.enable_wifi(devices)
                time.sleep(2)
                self.connect_wifi(devices, "Tomato", "hyraxSLB2018")
                self.wait_until_wifi_connect(devices)

            for form in formation_list:
                Adb.send_intent(devices, C.FORMATION_ENABLE,
                                [(C.ARG_TYPE, form),
                                 (C.ARG_DIS_LM_LW, "4000"),
                                 (C.ARG_DIS_LM_HG, "5000"),
                                 (C.ARG_VIS_LM_LW, "9000"),
                                 (C.ARG_VIS_LM_HG, "10000"),
                                 (C.ARG_CONN_LM_LW, str(group_size - 1)),
                                 (C.ARG_CONN_LM_HG, str(group_size - 1)),
                                 (C.ARG_IDLE_TIME, "18000"),
                                 (C.ARG_ACCEPT_PROB, "0.2")],
                                delay)
                time.sleep(1)

            y = input("Continue? [y/n]: ")

            Log.debug("Stopping network and App ...")
            # sends stop network intent
            Adb.send_intent(devices, C.NETWORK_STOP, [
                (C.ARG_TO_STOP, str(True)),
                (C.ARG_DELAY, "5000"),
            ])
            time.sleep(6)
            #Adb.stop_app(devices, C.PACKAGE)
            time.sleep(1)
            if y == 'y':
                # device remote path
                rmt_path = C.REMOTE_PATH + log_filename
                for dev in devices:
                    Adb.pull_file([dev], rmt_path, folder +
                                  "%s_%d_%d.log" % (dev, delay, i + 1))
            else:
                break
            i += 1

    def perform_route(self, number, group_size, routing, formation_list, f_pause,
                      listen_port, delay, shuffle):
        devices = Adb.get_devices()
        if len(devices) < number:
            raise Exception("Not enough connected devices")
        devices = devices[:number]
        if shuffle:
            random.shuffle(devices)
        print(devices)
        log_filename = "network.log"

        # start android app
        Adb.start_app(devices, C.ACTIVITY)
        time.sleep(2)

        print("Network init with %s" % (routing))
        # sends the start network intent
        Adb.send_intent(devices, C.NETWORK_START, [
            (C.ARG_TYPE, routing),
            (C.ARG_LOG_FILE_NAME, log_filename),
            (C.ARG_LISTEN_PORT, listen_port)
        ])
        time.sleep(2)

        formation = self.enable_formation(
            devices, group_size, formation_list, delay)
        folder = C.LOGS_FOLDER + "%s/%s/%d/" % (routing, formation, number)
        # creates logs folder of not existsd
        os.makedirs(folder, 0o777, True)

        # number of packets
        pkt_n = 30  # 60
        # variation of packets length
        # 1024, 4096, 16384, 32768, 65536
        # 512, 1024, 2048, ..., 16384, 32768  4096,
        pkt_lens = [1024, 2048, 4096]
        # variation of packets rate
        pkt_rates = [16, 32, 64]  # 1, 2, 4,  8, 16, 32,
        # number of hops
        net_hops = [3]  # 2

        # device remote path
        rmt_path = C.REMOTE_PATH + log_filename

        w = input("Run the packet tests? [y/n]: ")
        if w == 'y':
            if f_pause:
                Adb.send_intent(devices, C.FORMATION_PAUSE)
            # performs the all tests combination
            for pkt_len in pkt_lens:
                for pkt_rate in pkt_rates:
                    for hops in net_hops:
                        # TODO - for bluetooth just comment next line
                        p = pkt_n * pkt_rate
                        self.run_pkt_test(
                            devices, p, pkt_len, pkt_rate, hops, number)

                # print("Retrieving logs for " + str(pkt_len))
                # for dev in devices:
                #     Adb.pull_file([dev], rmt_path, folder +
                #                   "%s_%d.log" % (dev, pkt_len))

            w = input("Retrive log files? [y/n]: ")
            if (w == 'y'):
                for dev in devices:
                    Adb.pull_file([dev], rmt_path, folder + "%s.log" % (dev))

        if "wifi_hr_mix" in formation_list:
            self.disconnect_wifi(devices, "Tomato")
            self.disable_wifi(devices)
        # sends stop network intent
        Adb.send_intent(devices, C.NETWORK_STOP)
        time.sleep(2)

        print("Stopping the App ...")
        Adb.stop_app(devices, C.PACKAGE)

    def enable_formation(self, devices, group_size, formation_list, delay):
        if "wifi_hr_mix" not in formation_list:
            formation = ""
            # sends the formation algoritmhs
            for form in formation_list:
                print("Enabling formation %s" % (form))
                Adb.send_intent(devices, C.FORMATION_ENABLE,
                                [(C.ARG_TYPE, form)], delay)
                formation += form if formation == "" else "+" + form
                if len(formation_list) > 1:
                    w = input("Continue next formation?")
                time.sleep(2)
            return formation
        else:
            if group_size <= 1:
                raise Exception("Group size must be greater than 1")
            n_groups = math.ceil(len(devices) / group_size)
            Log.debug("Number of Groups %d with size %d" %
                      (n_groups, group_size))
            gos = []
            for i in range(0, len(devices), group_size):
                gos.append(devices[i])

            Log.debug("Gos connecting to Tomato...")
            self.enable_wifi(devices)
            time.sleep(5)
            self.connect_wifi(gos, "Tomato")
            time.sleep(7)
            Adb.send_intent(gos, C.FORMATION_ENABLE,
                            [(C.ARG_TYPE, C.FORMATION_WIFI_MESH_RANDOM)],
                            delay)  # wifi mesh
            c = input("Continue? [Y/n]: ")
            if c == 'y':
                time.sleep(1)
                for i in range(0, len(devices), group_size):
                    go = devices[i]
                    # ----- enable 'go' formation -----
                    # enable wifi direct star. Enforce group owner
                    Adb.send_intent([go], C.FORMATION_ENABLE,
                                    [(C.ARG_TYPE, C.FORMATION_WIFI_DIRECT_STAR),
                                     (C.ARG_DIS_LM_LW, "200"),
                                     (C.ARG_DIS_LM_HG, "300"),
                                     (C.ARG_CONN_LM_LW, str(group_size - 1)),
                                     (C.ARG_CONN_LM_HG, str(group_size - 1)),
                                     (C.ARG_IDLE_TIME, "25000"),
                                     (C.ARG_ACCEPT_PROB, "1")],
                                    delay)
                    time.sleep(4)
                    clients = devices[(i + 1):(i + group_size)]
                    self.disconnect_wifi(clients, "Tomato")
                    # ----- now enable the group peers ----
                    # enable wifi direct star. Enforce client
                    Adb.send_intent(clients, C.FORMATION_ENABLE,
                                    [(C.ARG_TYPE, C.FORMATION_WIFI_DIRECT_STAR),
                                     (C.ARG_ACCEPT_PROB, "0")],
                                    delay)
                    # wait for the client to connect to go
                    self.wait_until_p2p_connect(clients)
                    time.sleep(3)
            return "WIFI_MIX_DIRECT_" + str(group_size)

    def wait_until_wifi_connect(self, devices):
        def barrier_callback(dev):
            """Function that verifies if the property rule has been met"""
            while not Adb.has_ip_assigned(dev, "wlan0"):
                time.sleep(1)

        # blocks until all devices have assined ip address
        Adb.exec_command_parallel(devices, barrier_callback)

    def wait_until_p2p_connect(self, devices):
        def barrier_callback(dev):
            """Function that verifies if the property rule has been met"""
            while not Adb.has_ip_assigned(dev, "p2p-wlan0-*"):
                time.sleep(1)
        # blocks until all devices have assined ip address
        Adb.exec_command_parallel(devices, barrier_callback)

    def run_pkt_test(self, devices, pkt_n, pkt_len, pkt_rate, hops, n_devices):
        print(str(datetime.now()))
        print("Running packet test Packs (Packs: %d Len: %d Rate: %d Hops: %d Devices: %d) ..." %
              (pkt_n, pkt_len, pkt_rate, hops, n_devices))

        Adb.send_intent(devices, C.SEND_PACKETS, [
            (C.ARG_PKT_NUMBER, str(pkt_n)), (C.ARG_PKT_LENGTH, str(pkt_len)),
            (C.ARG_PKT_RATE, str(pkt_rate)), (C.ARG_NET_HOPS, str(hops))])

        # wait for the devices to perform the experiment
        key = "done_%d_%d_%d" % (pkt_len, pkt_rate, hops)
        Adb.perform_barrier(devices, key, '*', C.BARRIER_FILE)
        print("... done")

        # sleep 30 seconds times number of seconds
        # TODO - change for Wifi

        if pkt_rate > 16:
            time.sleep(6 * n_devices)
        else:
            time.sleep(4 * n_devices)

    def is_done_prop_valid(self, devices, barrier_key):
        """Verifies if the experiment execution ended with success.
        Args:
        devices -- a list of devices
        """
        for d in devices:
            v = Adb.get_property(d, C.BARRIER_DONE, C.BARRIER_FILE, 'false')
            if v == 'false':
                return False
        return True

    def send_intent_start(self, device, exp, log_file):
        """Sends a start intent to a device
        Args:
        device   -- device to send the intent start
        exp      -- the type of experiment
        log_file -- file log name
        """
        Adb.send_intent([device],  C.INTENT_START, [
            (C.ARG_EXP, exp),
            (C.ARG_LOG_FILE_NAME, log_file)
        ])


if __name__ == "__main__":
    # execute only if run as a script
    cmd = Cmd()
    cmd.execute(sys.argv[1:])
