from helper import Helper
from consts import Consts as C
import time


class Charts:
    @staticmethod
    def validate_args(keys, **kargs):
        for (k, v) in kargs:
            if k not in keys:
                raise Exception("%s key not expected" % k)

    @staticmethod
    def get_form_aggr_charts(all_stats):
        charts = []
        n_devs = list(all_stats.keys())
        n_devs.sort()
        charts.append(Charts.get_form_net_times_aggr(all_stats, n_devs))
        charts.extend(Charts.get_form_group_stats_aggr(all_stats, n_devs))
        charts.extend(Charts.get_form_net_stats_aggr(all_stats, n_devs))
        return charts

    @staticmethod
    def get_form_charts(stats, n_devs):
        delays = stats['delays']
        charts = []
        charts.append(Charts.get_form_net_times(stats))
        charts.append(Charts.get_form_group_stats(stats, n_devs))
        charts.extend(Charts.get_form_net_stats(stats, n_devs))
        return charts

    @staticmethod
    def get_form_net_times_aggr(all_stats, n_devs):
        series = []
        outliers = []
        # colors = ['#598234', '#AEBD38', '#68829E', '#505160']
        colors = ['#3F681C', '#FFBB00', '#FB6542', '#375E97']
        idx = 0
        for d in all_stats[n_devs[0]]['delays']:
            color = colors[idx]
            series.append({'name': "D%d" % d,
                           'data': [], 'color': color})
            outliers.append({'name': 'Outlier_%d' % d, 'color': color, 'type': 'scatter',
                         'data': [], 'showInLegend': False,
                         'marker': {'fillColor': 'white', 'lineWidth': 1, 'lineColor': color},
                         })
            idx += 1

        idx = 0
        for n in n_devs:
            if n != 20:
                continue
            chart = Charts.get_form_net_times(all_stats[n])
            data = chart['series'][0]['data']
            for i in range(len(data)):
                series[i]['data'].append(data[i])

            out = chart['series'][1]['data']

            m = [-0.225, -0.075, 0.075, 0.225]
            for [pos, o] in out:
                outliers[pos]['data'].append([idx + m[pos], o])

            idx += 1

        series.extend(outliers)
        return {
            'title': "Network Time",
            'type': "plotBoxPlot",
            'x': {'title': "# Devices", "categories": [20]}, # n_devs
            'y': {'title': "Time(s)"},
            'series': series
        }

    @staticmethod
    def get_form_net_times(stats):
        delays = stats['delays']
        cats = []
        series = [
            {'name': "Connections(First)", 'data': [], 'color': '#1E88E5'},
            {'name': 'Outlier(First)', 'color': '#1E88E5', 'type': 'scatter',
             'data': [],
             'marker': {'fillColor': 'white', 'lineWidth': 1, 'lineColor': '#1E88E5'},
             }
        ]

        i = 0
        for d in delays:
            cats.append(d)
            first = stats[d]['aggr']['stats']['conn_first']
            series[0]['data'].append([first['lw_wsk'], first['q1'],
                                      first['q2'], first['q3'], first['up_wsk']])
            for o in first['out']:
                series[1]['data'].append([i, o])
            i += 1

        return {
            'title': "Connections Times",
            'type': "plotBoxPlot",
            'x': {'title': "Delay (s)", "categories": cats},
            'y': {'title': "Time(s)"},
            'series': series
        }

    @staticmethod
    def get_form_group_stats_aggr(all_stats, n_devs):
        series = []
        colors = ['#598234', '#AEBD38', '#68829E', '#505160']
        j = 0
        for d in all_stats[n_devs[0]]['delays']:
            series.append({'name': "G%d" % (d), 'data': [], 'color': colors[j]})
            j += 1

        series.append({"name": "Best Conf", "data": [], 'color': '#FB6542'})

        for n in n_devs:
            series[-1]['data'].append(n // 5)
            chart = Charts.get_form_group_stats(all_stats[n], n)
            data = chart['series'][0]['data']
            for i in range(len(data)):
                series[i]['data'].append(data[i])
        return [
            {
                'title': "Number of Groups (Spline)",
                'type': "plotSpline",
                'x': {'title': "# Devices", "categories": n_devs},
                'y': {'title': "# Number", 'type': 'linear', 'plotLines': []},
                'series': series,
            },
            {
                'title': "Number of Groups (Column)",
                'type': "plotColumn",
                'x': {'title': "# Devices", "categories": n_devs},
                'y': {'title': "# Number", 'type': 'linear', 'plotLines': []},
                'stacking': 'normal',
                'series': series,
            },
        ]

    @staticmethod
    def get_form_group_stats(stats, n_devs):
        delays = stats['delays']

        cats = []
        group_data = []
        for d in delays:
            cats.append(d)
            group = stats[d]['aggr']['stats']['group']
            group_data.append(group['avg'])

        series = [
            {'name': "Groups", 'data': group_data},
        ]
        plotLines = [
            {'value': n_devs // 5, 'color': 'green', 'dashStyle': 'shortdash', 'width': 2,
                'label': {'text': ""}, 'zIndex': 5,
             }
        ]
        return {
            'title': "Formation Group",
            'type': "plotColumn",
            'x': {'title': "Delay (s)", "categories": cats},
            'y': {'title': "N", 'type': 'linear', 'plotLines': plotLines},
            'stacking': 'normal',
            'series': series,
        }

    @staticmethod
    def get_form_net_stats_aggr(all_stats, n_devs):
        series_ops = []
        series_tm = []
        colors = [
            "#C4DFE6", "#66A5AD", "#07575B", "#004445",
            "#A2C523", "#598234", "#486B00", "#2E4600",
            "#F8A055", "#FA6E59", "#AF4425", "#662E1C",
            "#EE693F", "#ED5752", "#CF3721", "#CB0000",
            "#F4CC70", "#DE7A22", "#9B4F0F", "#8D230F",
        ]
        # "#F1F1F2", "#BCBABE", "#A1D6E2", "#1995AD",
        # "#AEBD38", "#598234", ""
        # "#6FB98F", "#2C7873", "#004445", "#021C1E",
        # "#BA5536", "#A43820", "#693D3D", "#46211A",
        # "#EE693F", "#ED5752", "#CF3721", "#CB0000"

        "#F8A055", "#FA6E59", "#AF4425", "#662E1C"
        j = 0
        for d in all_stats[n_devs[0]]['delays']:
            series_ops.append({'name': "Disc D%d" % (d), 'data': [], 'stack': d, 'color': colors[0 + j]})
            series_ops.append({'name': "Conn D%d" % (d), 'data': [], 'stack': d, 'color': colors[4 + j]})
            series_ops.append({'name': "Fail D%d" % (d), 'data': [], 'stack': d, 'color': colors[8 + j]})


            series_tm.append({'name': "S%d" % (d), 'data': [], 'stack': d, 'color': colors[0 + j], 's': 2})
            series_tm.append({'name': "C%d" % (d), 'data': [], 'stack': d, 'color': colors[4 + j], 's': 1})
            series_tm.append({'name': "F%d" % (d), 'data': [], 'stack': d, 'color': colors[8 + j], 's': 0})
            j += 1

        for n in n_devs:
            chart = Charts.get_form_net_stats(all_stats[n], n)
            # operations
            series = chart[1]['series']
            for i in range(len(series)):
                data = series[i]['data']
                for j in range(len(data)):
                    idx = i + j * 3
                    series_ops[idx]['data'].append(data[j])

            # times avg
            series = chart[2]['series']
            for i in range(len(series)):
                data = series[i]['data']
                for j in range(len(data)):
                    idx = i + j * 3
                    series_tm[idx]['data'].append(data[j])

        series_tm = sorted(series_tm, key=lambda x: x['s']);
        #series_tm = list(filter(lambda x: x['s'] != 2, series_tm))

        return [{
                'title': "Network Operations Per Device",
                'type': "plotStackedGroup",
                'x': {'title': "# devices", "categories": n_devs},
                'y': {'title': "# Number", 'type': 'linear', 'plotLines': []},
                'stacking': 'normal',
                'series': series_ops,
            }, {
                'title': "Time Spend on Each Operation (Avg)",
                'type': "plotStackedGroup",
                'x': {'title': "# Devices", "categories": n_devs},
                'y': {'title': "Time(s)", 'type': 'linear', 'plotLines': []},
                'stacking': 'normal',
                'series': series_tm,
            },
        ]

    @staticmethod
    def get_number_of_clients(stats, delay, n_devs):
        reps = stats['reps']
        c = 0
        g = 0
        t = 0
        for r in reps:
            g += stats[delay][r]['stats']['group']['avg']
            c += (n_devs - stats[delay][r]['stats']['group']['avg'])
            t += n_devs
        return (c, g, t)

    @staticmethod
    def get_form_net_stats(stats, n_devs):
        delays = stats['delays']
        cats = []
        conn_ts_data, fail_ts_data, dis_ts_data = [], [], []
        conn_ops, fail_ops, dis_ops = [], [], []
        conn_tm, fail_tm, dis_tm = [], [], []

        for d in delays:
            cats.append(d)
            (n_c, n_g, t) = Charts.get_number_of_clients(stats, d, n_devs)
            conn_avg = stats[d]['aggr']['stats']['conn_success']['avg']
            fail_avg = stats[d]['aggr']['stats']['conn_fails']['avg']
            dis_avg = stats[d]['aggr']['stats']['dis']['avg']
            conn_ts_data.append(conn_avg)
            fail_ts_data.append(fail_avg)
            dis_ts_data.append(dis_avg)

            n_conn = stats[d]['aggr']['stats']['conn_success']['cnt']
            n_fail = stats[d]['aggr']['stats']['conn_fails']['cnt']
            n_dis = stats[d]['aggr']['stats']['dis']['cnt']

            conn_dev = n_conn / n_c
            fail_dev = n_fail / n_c
            dis_dev = (n_dis - n_g) / n_c
            conn_ops.append(conn_dev)
            fail_ops.append(fail_dev)
            dis_ops.append(dis_dev)

            conn_tm.append(conn_avg * conn_dev)
            fail_tm.append(fail_avg * fail_dev)
            dis_tm.append(dis_avg * dis_dev)

        series_ts = [
            {'name': "Discovery", 'data': dis_ts_data, 'color': '#64B5F6'},
            {'name': "Conn Success", 'data': conn_ts_data, 'color': '#81C784'},
            {'name': "Conn Failures", 'data': fail_ts_data, 'color': '#E57373'},
        ]

        series_ops = [
            {'name': "Discovery", 'data': dis_ops, 'color': '#64B5F6'},
            {'name': "Conn Success", 'data': conn_ops, 'color': '#81C784'},
            {'name': "Conn Failures", 'data': fail_ops, 'color': '#E57373'},
        ]

        series_tm = [
            {'name': "Discovery", 'data': dis_tm, 'color': '#64B5F6'},
            {'name': "Conn Success", 'data': conn_tm, 'color': '#81C784'},
            {'name': "Conn Failures", 'data': fail_tm, 'color': '#E57373'},
        ]

        return [{
            'title': "Network times",
            'type': "plotStackedGroup",
            'x': {'title': "Delay (s)", "categories": cats},
            'y': {'title': "Time (s)", 'type': 'linear', 'plotLines': []},
            'stacking': 'normal',
            'series': series_ts,
        }, {
            'title': "Network Operation per Device",
            'type': "plotStackedGroup",
            'x': {'title': "Delay (s)", "categories": cats},
            'y': {'title': "# ops", 'type': 'linear', 'plotLines': []},
            'stacking': 'normal',
            'series': series_ops,
        }, {
            'title': "Time Spend on Each Operation (Avg)",
            'type': "plotStackedGroup",
            'x': {'title': "Delay (s)", "categories": cats},
            'y': {'title': "Time(s)", 'type': 'linear', 'plotLines': []},
            'stacking': 'normal',
            'series': series_tm,
        }
        ]

        # colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
        #           '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
        #           '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
        #           '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
        #           # purple + deep purple
        #           '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C',
        #           # orange + deep orange
        #           '#FFE0B2', '#FF8A65', '#FFA726', '#FF5722', '#F57C00', '#D84315', '#E65100',
        #           '#D7CCC8', '#BCAAA4', '#BCAAA4', '#8D6E63', '#5D4037', '#4E342E', '#3E2723',  # brown
        #           ]

    # @staticmethod
    # def get_chart_group_aggr(stats_map):
    #     cats = sorted(stats_map.keys(), key=lambda x: x)
    #     pkts_len = set(list(map(lambda x: x['len'], stats_map[cats[0]])))
    #     hops = set(list(map(lambda x: x['hops'], stats_map[cats[0]])))
    #     pkts_len, hops = sorted(pkts_len), sorted(hops)
    #
    #     combs = [(l, h) for l in pkts_len for h in hops]
    #     charts = list(map(lambda x: Charts.get_latency_chart_aggr(
    #         stats_map, cats, x[0], x[1]), combs))
    #
    #     charts.extend(
    #         list(map(lambda x: Charts.get_latency_chart_bar_aggr(
    #             stats_map, cats, x), pkts_len))
    #     )
    #     return charts

    @staticmethod
    def get_chart_group_aggr(stats_map):
        cats = sorted(stats_map.keys(), key=lambda x: x)
        stats = stats_map[cats[-1]]
        pkts_len = set(list(map(lambda x: x['len'], stats)))
        pkts_len = sorted(pkts_len)
        hp = max(list(map(lambda x: x['hops'], stats)))
        hops = []
        for h in C.HOPS:
            for s in stats:
                if s['latency_stats'][h]['cnt'] > 0:
                    hops.append((h, "_H" + h))
                    break
        keys = hops
        charts = []
        pkts_len = pkts_len if pkts_len[0] != 512 else pkts_len[1:]
        for p_len in pkts_len:
            charts.append(Charts.get_latency_chart_bar_aggr(
                stats_map, cats, p_len, hp, keys, 'avg'))
        charts.append(Charts.get_traffic_bar_aggr(stats_map, cats, hp))

        return charts

    @staticmethod
    def get_latency_chart_bar_aggr(stats_map, cats, pkt_len, h, keys, stat):
        series_map = {}
        series = []
        i = 0
        shift = 7
        rate_map = {1: 0, 2: 1, 4: 2, 8: 3, 16: 4, 32: 5, 64: 6}

        colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
                  '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
                  '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
                  '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
                  # purple + deep purple
                  '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C',
                  ]

        for c in cats:
            stats = list(
                filter(lambda x: x['len'] == pkt_len and x['hops'] == h, stats_map[c]))
            for j in range(len(keys)):
                (k, v) = keys[j]
                last = (j == (len(keys) - 1))
                for s in stats:
                    key = "R%d%s" % (s['rate'], v)
                    idx = series_map.setdefault(key, i)
                    if idx == i:
                        pos = rate_map[s['rate']] + (j * shift)
                        if last:
                            series.append({'name': key + "avg", 'data': [],
                                           'showInLegend': False,
                                           'color': '#9E9E9E', 'stack': key})

                        series.append({'name': key, 'data': [],
                                       'color': colors[pos], 'stack': key})

                        i += 2 if last else 1
                    val = s['latency_stats'][k][stat]
                    if last:
                        alt = s['latency_stats']['full'][stat] if val > 0 else 0
                        series[idx]['data'].append((val - alt) / 1000.0)
                        series[idx + 1]['data'].append(alt / 1000.0)
                    else:
                        series[idx]['data'].append(val / 1000.0)

        return {
            'title': "Latency %d" % (pkt_len),
            'type': "plotStackedGroup",
            'x': {'title': "# devices", "categories": cats},
            'y': {'title': "Latency (s)", 'type': 'logarithmic'},
            'stacking': 'normal',
            'series': series,
        }

    @staticmethod
    def get_traffic_bar_aggr(stats_map, cats, h):
        tag = 'all'
        series_map = {}
        series = []
        i = 0
        shift = 7
        rate_map = {1: 0, 2: 1, 4: 2, 8: 3, 16: 4, 32: 5, 64: 6}
        len_map = {1024: 0, 2048: 1,
                   4096: 2, 8192: 3, 16384: 4, 32768: 5}
        colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
                  '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
                  '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
                  '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
                  # purple + deep purple
                  '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C',
                  # orange + deep orange
                  '#FFE0B2', '#FF8A65', '#FFA726', '#FF5722', '#F57C00', '#D84315', '#E65100',
                  '#D7CCC8', '#BCAAA4', '#BCAAA4', '#8D6E63', '#5D4037', '#4E342E', '#3E2723',  # brown
                  ]
        for c in cats:
            stats = list(filter(lambda x: x['hops'] == h, stats_map[c]))
            j = 0
            for s in stats:
                if s['len'] in [512]: #
                    continue
                key = "R%d_%dK" % (s['rate'], int(s['len'] / 1000))
                idx = series_map.setdefault(key, i)
                if idx == i:
                    pos_z = shift * len_map[s['len']]
                    pos = rate_map[s['rate']] + pos_z
                    series.append({'name': key + "over", 'data': [],
                                   'showInLegend': False,
                                   'color': '#9E9E9E', 'stack': key})
                    series.append({'name': key, 'data': [],
                                   'color': colors[pos], 'stack': key})

                    i += 2
                val = s['traffic'][tag]['route'] - s['traffic'][tag]['sent']
                series[idx]['data'].append(val / (1024 * 1024))
                val = s['traffic'][tag]['sent']
                series[idx + 1]['data'].append(val / (1024 * 1024))
                j += 1
        return {
            'title': "Traffic Sent",
            'type': "plotStackedGroup",
            'x': {'title': "# Devices", "categories": cats},
            'y': {'title': "Traffic (MB)", 'type': "logarithmic"},
            'stacking': 'normal',
            'series': series,
        }

    @staticmethod
    def get_chart_group(stats):
        h = max(list(map(lambda x: x['hops'], stats)))
        f_stats = list(filter(lambda x: x['hops'] == h, stats))
        charts = []
        hops = []
        for h in C.HOPS:
            for s in stats:
                if s['latency_stats'][h]['cnt'] > 0:
                    hops.append((h, "_H" + h))
                    break
        keys = [('full', '')] + hops
        charts.append(Charts.get_latency_chart_bar(f_stats, keys, 'avg', ''))
        charts.append(Charts.get_packets_bar(f_stats, hops, 'cnt'))
        charts.append(Charts.get_traffic_bar(f_stats))
        return charts

    @staticmethod
    def get_latency_chart_bar(stats, keys, stat, title):
        series_map = {}
        series = []
        cats = set()
        i = 0
        shift = 7
        rate_map = {1: 0, 2: 1, 4: 2, 8: 3, 16: 4, 32: 5, 64: 6}
        colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
                  '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
                  '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
                  '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
                  # purple + deep purple
                  '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C',
                  ]
        for j in range(len(keys)):
            (k, v) = keys[j]
            for s in stats:
                cats.add(s['len'])
                key = "R%d%s" % (s['rate'], v)
                idx = series_map.setdefault(key, i)
                if idx == i:
                    pos = rate_map[s['rate']] + (j * shift)
                    series.append(
                        {'name': key, 'data': [], 'color': colors[pos]})
                    i += 1
                val = s['latency_stats'][k][stat]
                series[idx]['data'].append(val / 1000.0)

        cats = list(cats)
        cats.sort()
        return {
            'title': "Latency %s" % (title),
            'type': "plotColumn",
            'x': {'title': "Packets Len", "categories": cats, 'tick': True},
            'y': {'title': "Latency (s)"},
            'series': series,
        }

    @staticmethod
    def get_packets_bar(stats, keys, stat):
        series_map = {}
        series = []
        cats = set()
        i = 0
        shift = 7
        rate_map = {1: 0, 2: 1, 4: 2, 8: 3, 16: 4, 32: 5, 64: 6}
        colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
                  '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
                  '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
                  '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
                  # purple + deep purple
                  '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C',
                  ]
        for j in range(len(keys)):
            (k, v) = keys[j]
            for s in stats:
                cats.add(s['len'])
                key = "R%d%s" % (s['rate'], v)
                idx = series_map.setdefault(key, i)
                if idx == i:
                    pos = rate_map[s['rate']] + ((j + 1) * shift)
                    series.append({'name': key, 'data': [], 'color': colors[pos],
                                   'stack': str(s['rate'])})
                    i += 1
                val = s['latency_stats'][k][stat]
                series[idx]['data'].append(val)

        cats = list(cats)
        cats.sort()
        return {
            'title': "Percentage of packets per hop",
            'type': "plotStackedGroup",
            'x': {'title': "Packets Len", "categories": cats},
            'y': {'title': "Packets(%)", 'type': "linear"},
            'stacking': 'percent',
            'series': series,
        }

    @staticmethod
    def get_traffic_bar(stats):
        tag = 'all'
        series_map = {}
        series = []
        cats = set()
        i = 0
        shift = 7
        rate_map = {1: 0, 2: 1, 4: 2, 8: 3, 16: 4, 32: 5, 64: 6}
        colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
                  '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
                  '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
                  '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
                  # purple + deep purple
                  '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C',
                  ]
        # keys = [('sent', '_sent'), ('rcv', '_rcv')]
        for s in stats:
            cats.add(s['len'])
            key = "R%d" % (s['rate'])
            idx = series_map.setdefault(key, i)
            if idx == i:
                pos = rate_map[s['rate']]
                series.append({'name': key, 'data': [], 'color': colors[pos],
                               'stack': str(s['rate'])})
                i += 1
            val = s['traffic'][tag]['sent'] + s['traffic'][tag]['rcv']
            series[idx]['data'].append(val / (1024 * 1024))

        cats = list(cats)
        cats.sort()
        return {
            'title': "Traffic Sent + Rcv",
            'type': "plotStackedGroup",
            'x': {'title': "Packets Len", "categories": cats},
            'y': {'title': "Traffic (MB)", 'type': "logarithmic"},
            'stacking': 'normal',
            'series': series,
        }

    @staticmethod
    def get_cpu_chart(cpu, stats):
        all_cpu, proc_cpu = [], []
        plotLines = []
        for (ts, total, proc) in cpu:
            time = ts / 10**6
            all_cpu.append({'x': time, 'y': (total - proc)})
            proc_cpu.append({'x': time, 'y': proc})

        series = []
        series.append({'name': "Remain Cpu", 'data': all_cpu})
        series.append({'name': "Process Cpu", 'data': proc_cpu})
        return {
            'title': "Cpu over time",
            'type': "plotStackedArea",
            'x': {'title': "Time", 'plotLines': plotLines},
            'y': {'title': "Percentage(%)"},
            'stacking': 'normal',
            'series': series,
        }

    @staticmethod
    def get_mem_chart(mem, stats):
        rss_mem, shared_mem = [], []
        plotLines = []
        for (ts, total, rss, shared) in mem:
            time = ts / 10**6
            rss_mem.append({'x': time, 'y': rss})
            shared_mem.append({'x': time, 'y': shared})

        # for s in stats:
        #     key = "%d|%d|%d|%d" %(s['n'], s['len'], s['rate'], s['hops'])
        #     plotLines.append({
        #         'color': '#FF0000',
        #         'width': 1,
        #         'value': (s['start_ts'] / 10**6),
        #         'label': {
        #             'text': key
        #         }
        #     })

        series = []
        series.append({'name': "Rss Memory", 'data': rss_mem})
        series.append({'name': "Shared Memory", 'data': shared_mem})
        return {
            'title': "Memory over time",
            'type': "plotStackedArea",
            'x': {'title': "Time", 'plotLines': plotLines},
            'y': {'title': "Size (KB)"},
            'stacking': 'normal',
            'series': series,
        }

    @staticmethod
    def get_hw_table(cpu, mem, stats):
        table = []
        c_pos, c_len = 0, len(cpu)
        for s in stats:
            start_ts, end_ts = s['start_ts'], s['end_ts']
            data = {'pkts': s['n'], 'len': s['len'], 'rate': s['rate'],
                    'hops': s['hops']}
            cpu_total, cpu_proc = [], []
            mem_rss, mem_shared = [], []
            while c_pos < c_len:
                (ts, total, proc) = cpu[c_pos]
                (_, _, rss, shared) = mem[c_pos]
                c_pos += 1
                if ts < start_ts:
                    continue
                if ts >= start_ts and ts <= end_ts:

                    cpu_total.append(total * 100)
                    cpu_proc.append(proc * 100)
                    mem_rss.append(rss / (1024 * 1024))
                    mem_shared.append(shared / (1024 * 1024))
                else:
                    break
            data["cpu_total"] = Helper.stats_from_list(cpu_total)
            data["cpu_proc"] = Helper.stats_from_list(cpu_proc)
            data["mem_rss"] = Helper.stats_from_list(mem_rss)
            data["mem_shared"] = Helper.stats_from_list(mem_shared)

            table.append(data)
        return table
