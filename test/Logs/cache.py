import json
from pymemcache.client.base import Client
import lz4.frame


class Cache:
    def __init__(self, server, port):
        def json_serializer(key, value):
            if type(value) == str:
                return value, 1
            return json.dumps(value), 2

        def json_deserializer(key, value, flags):
            if flags == 1:
                return value.decode('utf-8')
            if flags == 2:
                return json.loads(value.decode('utf-8'))
            raise Exception("Unknown serialization format")

        self.client = Client((server, port))
                             #serializer=json_serializer,
                             #deserializer=json_deserializer)

    def get(self, key):
        data_comp = self.client.get(key)
        if data_comp is not None:
            data_bytes = lz4.frame.decompress(data_comp)
            return json.loads(data_bytes.decode())
        return None

    def has_key(self, key):
        return self.client.get(key) != None

    def set(self, key, value):
        dump = json.dumps(value)
        comp = lz4.frame.compress(dump.encode(), compression_level=16)
        print("Orig: %d Comp: %d" %(len(dump), len(comp)))
        self.client.set(key, comp)

    def clear(self):
        self.client.flush_all()

    def close(self):
        self.client.close()
