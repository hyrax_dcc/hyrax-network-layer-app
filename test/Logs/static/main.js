(function(H) {
    function symbolWrap(proceed, symbol, x, y, w, h, options) {
        if (symbol.indexOf('text:') === 0) {
            var text = symbol.split(':')[1],
                svgElem = this.text(text, x, y + h)
                .css({
                    fontFamily: 'FontAwesome',
                    fontSize: h * 2
                });

            if (svgElem.renderer.isVML) {
                svgElem.fillSetter = function(value, key, element) {
                    element.style.color = H.Color(value).get('rgb');
                };
            }
            return svgElem;
        }
        return proceed.apply(this, [].slice.call(arguments, 1));
    }
    H.wrap(H.SVGRenderer.prototype, 'symbol', symbolWrap);
    if (H.VMLRenderer) {
        H.wrap(H.VMLRenderer.prototype, 'symbol', symbolWrap);
    }

    // Load the font for SVG files also
    H.wrap(H.Chart.prototype, 'getSVG', function(proceed) {
        var svg = proceed.call(this);
        svg = '<?xml-stylesheet type="text/css" ' +
            'href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" ?>' +
            svg;
        return svg;
    });
    //H.Axis.prototype.allowNegativeLog = true;
}(Highcharts));

$(document).ready(function() {
    //var $table = $('#table');
});

/**
Arguments style:
{
  title: "<value>",
  x: {
    min: <value>
    max: <value>
    title: "<value>"
  },
  y: {
    min: <value>
    max: <value>
    title: "<value>"
  },
  series: [
    {name: "<serie_name>", data: {x <val>: , y: <val> ...}},
    ...
  ]
}
**/
function plotLine(divId, json) {
    Highcharts.chart(divId, {
        chart: {
            type: "line",
            //zoomType: 'xy'
            //zoomType: 'x',
        },
        title: {
            text: "", //json.title
        },
        xAxis: {
            title: {
                text: json.x.title
            },
            allowDecimals: false,
            tickPositions: (json.x.tick) ? json.x.categories : undefined,
            //categories: json.x.categories,
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: json.y.title
            },
            min: 0,
        },
        legend: {
            enabled: true,
        },
        tooltip: {
            backgroundColor: null,
            borderWidth: 0,
            shadow: false,
            useHTML: true,
            style: {
                padding: 0
            },
            tooltip: {
                formatter: function() {
                    return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series ' + this.series.name;
                }
            }
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                // marker: {
                //     enabled: false
                // }
            },
        },
        credits: {
            enabled: false
        },
        series: json.series,
    });
}

function plotSpline(divId, json) {
    Highcharts.chart(divId, {
        chart: {
            type: "spline",
            zoomType: 'y',
        },
        title: {
            text: "",
        },
        xAxis: {
            categories: json.x.categories,
            title: {
                text: json.x.title
            },
        },
        yAxis: {
            // allowDecimals: false,
            title: {
                text: json.y.title
            },
        },
        legend: {
            enabled: true,
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span> -> <p>{point.action}: ({point.y:,.3f}) - RRT: {point.diff} </p>',
            split: true
        },
        plotOptions: {
            line: { // <--- Chart type here, check the API reference first!

            },
            series: {
                turboThreshold: 0,
            }
        },
        credits: {
            enabled: false
        },
        series: json.series,
    });
}

function plotStackedArea(divId, json) {
    Highcharts.chart(divId, {
        chart: {
            type: "area",
            zoomType: 'x',
        },
        title: {
            text: "" //json.title
        },
        xAxis: {
            title: {
                text: json.x.title
            },
            type: 'datetime',
            plotLines: json.x.plotLines,
        },
        yAxis: {
            title: {
                text: json.y.title
            },
            min: 0,
        },
        legend: {
            enabled: true,
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>({point.y:,.3f})<br/>',
            split: true
        },
        plotOptions: {
            area: {
                marker: {
                    enabled: false
                }
            },
            series: {
                turboThreshold: 0,
                marker: {
                    enabled: false,
                },
            }
        },
        credits: {
            enabled: false
        },
        series: json.series,
    });
}

function plotColumn(divId, json) {
    hLines = [];
    if (json.y.hasOwnProperty('plotLines')) {
        hLines = json.y.plotLines;
    }
    Highcharts.chart(divId, {
        chart: {
            type: 'column'
        },
        title: {
            text: "" //json.title
        },
        xAxis: {
            categories: json.x.categories,
            crosshair: true,
            title: {
                text: json.x.title
            }
        },
        yAxis: {
            //min: 0,
            type: json.y.type,
            minorTickInterval: 'auto',
            title: {
                text: json.y.title
            },
            plotLines: hLines,
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} ' + json.y.unit + '</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                groupPadding: 0.05,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: true
        },
        series: json.series
    });
}

function plotStackedGroup(divId, json) {
    console.log(json);
    Highcharts.chart(divId, {
        chart: {
            type: 'column'
        },

        title: {
            text: "" //json.title
        },

        xAxis: {

            categories: json.x.categories
        },

        yAxis: {
            //allowDecimals: false,
            //min: 0,
            type: json.y.type,
            minorTickInterval: 'auto',
            title: {
                text: json.y.title
            }
        },

        tooltip: {
            formatter: function() {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {
                stacking: json.stacking,
                pointPadding: 0.2,
                groupPadding: 0.05,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: true,
            width: 1920,
            height: 720,
        },
        series: json.series
    });
}

function plotBoxPlot(divId, json) {
    Highcharts.chart(divId, {
      chart: {
          type: 'boxplot',
          zoomType: 'y',
          backgroundColor:"rgba(255, 255, 255, 0)",
          style: {
              fontSize: "20px",
          },
      },

      title: {
          text: "" //json.title
      },

      xAxis: {
          categories: json.x.categories,
          crosshair: true,
          maxPadding: 0.25,
          padding: 0.25,
          title: {
              text: json.x.title,
              style: {
                  fontSize: "20px",
                  color: "#000000",
              },
          },
          labels: {
                style: {
                		fontSize: "20px",
                    color: "#000000",
                }
            }
      },
      yAxis: {
          min: 0,
          type: json.y.type,
          minorTickInterval: 'auto',
          title: {
              text: json.y.title,
              style: {
                  fontSize: "20px",
                  color: "#000000",
              },
          },
          labels: {
                style: {
                		fontSize: "20px",
                    color: "#000000",
                }
            },
            max:80,
      },
      legend: {
          itemStyle: {
             fontSize: "20px",
              },
        },
      credits: {
          enabled: false,
      },
      exporting: {
          enabled: true,
          sourceWidth: 640,
          sourceHeight: 480,
          scale: 1,
      },
      series: json.series
    });
}
