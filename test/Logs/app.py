import os
import re
import sys
import time
from math import *
from flask import Flask
from flask import render_template
from flask import g

from helper import Helper
from form_helper import FormHelper
from charts import Charts
from cache import Cache
from statistics import *
from consts import Consts as C

app = Flask(__name__)


def get_cache():
    cache = getattr(g, '_cache', None)
    if cache is None:
        cache = g._cache = Cache("localhost", 11211)
    return cache


@app.teardown_appcontext
def teardown_cache(exception):
    cache = getattr(g, '_cache', None)
    if cache is not None:
        cache.close()


@app.route("/")
def index():
    return render_template('index.html', navs=Helper.get_navs())


@app.route("/<routing>/")
def index_routing(routing):
    navs = Helper.get_navs(routing)
    return render_template('index.html', navs=navs)


@app.route("/<routing>/<formation>")
def index_formation(routing, formation):
    navs = Helper.get_navs(routing, formation)
    return render_template('index.html', navs=navs)


@app.route("/<routing>/<formation>/aggregate")
def index_devs_aggr(routing, formation):
    navs = Helper.get_navs(routing, formation, 'aggregate')
    return render_template('index.html', navs=navs)

# top aggregate stats --------------------------------------------------------
@app.route("/formation/<formation>/aggregate/charts")
def index_form_aggr(formation):
    navs = Helper.get_navs("formation", formation, 'aggregate', 'charts')
    path = Helper.build_local_path('formation', formation)
    stats = FormHelper.get_form_aggr_all(path, get_cache())
    charts = Charts.get_form_aggr_charts(stats)
    return render_template('charts.html', navs=navs, charts=charts)

@app.route("/<routing>/<formation>/aggregate/charts")
def index_devs_aggr_latency(routing, formation):
    navs = Helper.get_navs(routing, formation, 'aggregate', 'charts')
    path = Helper.build_local_path(routing, formation)
    stats_map = Helper.get_aggr_stats_map(path, get_cache())

    charts = Charts.get_chart_group_aggr(stats_map)
    return render_template('charts.html', navs=navs, charts=charts)


@app.route("/<routing>/<formation>/<n_devs>")
def index_n_devs(routing, formation, n_devs):
    navs = Helper.get_navs(routing, formation, n_devs)
    return render_template('index.html', navs=navs)


@app.route("/<routing>/<formation>/<n_devs>/<dev>")
def index_dev(routing, formation, n_devs, dev):
    navs = Helper.get_navs(routing, formation, n_devs, dev)
    return render_template('index.html', navs=navs)


# table stats-------------------------------------------------------------------
@app.route("/formation/<formation>/<n_devs>/aggregate/table")
def form_dev_aggr_table(formation, n_devs):
    navs = Helper.get_navs("formation", formation, n_devs, 'aggregate', 'table')
    path = Helper.build_local_path('formation', formation, n_devs)
    stats = FormHelper.get_form_stats(path, get_cache())
    return render_template('form_table.html', navs=navs, stats=stats)

@app.route("/formation/<formation>/<n_devs>/aggregate/table_aggr")
def form_dev_aggr_table_aggr(formation, n_devs):
    navs = Helper.get_navs("formation", formation, n_devs, 'aggregate', 'table_aggr')
    path = Helper.build_local_path('formation', formation, n_devs)
    stats = FormHelper.get_form_stats_aggr(path, get_cache())
    return render_template('form_table_aggr.html', navs=navs, stats=stats)

@app.route("/<routing>/<formation>/<n_devs>/aggregate/table")
def index_dev_aggr_table(routing, formation, n_devs):
    navs = Helper.get_navs(routing, formation, n_devs, 'aggregate', 'table')
    path = Helper.build_local_path(routing, formation, n_devs)
    stats = Helper.get_merged_device_stats(path, get_cache())
    return render_template('dev_table.html', navs=navs, stats=stats)


@app.route("/<routing>/<formation>/<n_devs>/<dev>/table")
def index_dev_table(routing, formation, n_devs, dev):
    navs = Helper.get_navs(routing, formation, n_devs, dev, 'table')
    path = Helper.build_local_path(routing, formation, n_devs, dev)
    (_, _, stats) = Helper.get_device_stats(path, get_cache())
    return render_template('dev_table.html', navs=navs, stats=stats)


# charts -----------------------------------------------------------------
@app.route("/<routing>/<formation>/<n_devs>/aggregate/charts")
def index_dev_aggr_latency(routing, formation, n_devs):
    navs = Helper.get_navs(routing, formation, n_devs,
                           "aggregate", 'charts')
    path = Helper.build_local_path(routing, formation, n_devs)
    stats = Helper.get_merged_device_stats(path, get_cache())

    charts = Charts.get_chart_group(stats)
    return render_template('charts.html', navs=navs, charts=charts)

@app.route("/formation/<formation>/<n_devs>/aggregate/charts")
def form_dev_charts(formation, n_devs):
    navs = Helper.get_navs("formation", formation, n_devs, 'aggregate', 'charts')
    path = Helper.build_local_path('formation', formation, n_devs)
    stats = FormHelper.get_form_stats_aggr(path, get_cache())
    charts = Charts.get_form_charts(stats, int(n_devs))
    return render_template('charts.html', navs=navs, charts=charts)


@app.route("/<routing>/<formation>/<n_devs>/<dev>/charts")
def index_dev_latency(routing, formation, n_devs, dev):
    navs = Helper.get_navs(routing, formation, n_devs, dev, 'charts')
    path = Helper.build_local_path(routing, formation, n_devs, dev)
    (_, _, stats) = Helper.get_device_stats(path, get_cache())

    charts = Charts.get_chart_group(stats)
    return render_template('charts.html', navs=navs, charts=charts)


# hardware stats --------------------------------------------------------------------
@app.route("/<routing>/<formation>/<n_devs>/<dev>/charts_hw")
def index_dev_hw(routing, formation, n_devs, dev):
    navs = Helper.get_navs(routing, formation, n_devs, dev, 'charts_hw')
    path = Helper.build_local_path(routing, formation, n_devs, dev)
    (cpu, mem, stats) = Helper.get_device_stats(path, get_cache())

    cpu_stat = Charts.get_cpu_chart(cpu, stats)
    mem_stat = Charts.get_mem_chart(mem, stats)

    charts = [cpu_stat, mem_stat]
    table_hw = Charts.get_hw_table(cpu, mem, stats)
    return render_template('charts.html', navs=navs, charts=charts, table_hw=table_hw, col=12)
