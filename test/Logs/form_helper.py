from consts import Consts as C
from helper import Helper
import re
import time
import math


class FormHelper:

    @staticmethod
    def get_struct(path):
        files = Helper.list_files(path)
        devs = []
        delays = []
        reps = []
        for f in files:
            fl = re.split("_|\.", f)
            if fl[0] not in devs:
                devs.append(fl[0])
            if int(fl[1]) not in delays:
                delays.append(int(fl[1]))
            if int(fl[2]) not in reps:
                reps.append(int(fl[2]))
        devs.sort()
        delays.sort()
        reps.sort()
        return (devs, delays, reps)

    @staticmethod
    def get_form_aggr_all(parent_path, cache=None):
        folders = Helper.list_files(parent_path, False)
        fold_map = dict(map(lambda x:
                            (int(x),
                             FormHelper.get_form_stats_aggr("%s/%s" % (parent_path, x))),
                            folders))
        return fold_map

    @staticmethod
    def get_form_stats_aggr(path, cache=None):
        res = FormHelper.get_form_stats(path, cache)
        delays = res['delays']
        reps = res['reps']
        aggr = {'delays': delays, 'reps': reps}

        def _init():
            return {
                'conn_first': [],
                'conn_last': [],
                'conn_fails': [],
                'conn_success': [],
                'conn_server': [],
                'dis': [],
                'vis': [],
                'group': [],
                'stats': {},
            }

        def _close(aggr):
            m = {'stats': {}}
            for (_, d) in aggr.items():
                for (k, lv) in d.items():
                    if k == 'stats':
                        continue
                    if k not in m:
                        m[k] = []
                    m[k].extend(lv)
                    d['stats'][k] = Helper.stats_from_list(lv)

            for(k, lv) in m.items():
                if k == 'stats':
                    continue
                m['stats'][k] = Helper.stats_from_list(lv)
            aggr['aggr'] = m

        for d in delays:
            assert d not in aggr
            aggr[d] = {}
            for r in reps:
                assert r not in aggr[d]
                aggr[d][r] = _init()
                adjs = {}
                for data in res[d][r]:
                    adjs[data['addr']] = data['net']
                    aggr[d][r]['conn_first'].append(data['conn_first'])
                    aggr[d][r]['conn_last'].append(data['conn_last'])
                    aggr[d][r]['conn_fails'].extend(data['list']['conns'][0])
                    aggr[d][r]['conn_success'].extend(data['list']['conns'][1])
                    aggr[d][r]['conn_server'].append(data['list']['conns'][2])
                    aggr[d][r]['dis'].extend(data['list']['dis'])
                    aggr[d][r]['vis'].extend(data['list']['vis'])

                (n_groups, g_sizes) = FormHelper.calc_groups(adjs)
                aggr[d][r]['group'].append(n_groups)
            _close(aggr[d])
        return aggr

    @staticmethod
    def calc_groups(adjs):
        groups = []

        def get_group(key, adjs, vis=[]):
            if key in vis:
                return set()

            g = set()
            g.add(key)
            vis.append(key)
            for v in adjs[key]:
                g.add(v)
                r = get_group(v, adjs, vis)
                g.update(r)
            return g

        vis = []
        n_groups = 0
        g_sizes = []
        for k in list(adjs.keys()):
            if k not in vis:
                # g = []
                g = list(get_group(k, adjs, vis))
                groups.append(g)
                n_groups += 1
                g_sizes.append(len(g))

        return (n_groups, g_sizes)

    @staticmethod
    def get_form_stats(path, cache=None):
        (devs, delays, reps) = FormHelper.get_struct(path)
        res = {
            'delays': delays,
            'reps': reps,
            'devs': devs,
        }

        for d in delays:
            assert d not in res
            res[d] = {}
            for r in reps:
                assert r not in res[d]
                res[d][r] = []
                i = 1
                for dev in devs:
                    f = f = path + "/%s_%d_%d.log" % (dev, d, r)
                    (addr, start, end, first_conn, last_conn, dis, vis, server,
                     conn, net) = FormHelper.get_stats_from_file(f)

                    (fails, success, server_rcv) = FormHelper.get_conn_times(conn)
                    f_stats = Helper.stats_from_list(fails)
                    s_stats = Helper.stats_from_list(success)

                    d_times = FormHelper.get_times(dis, end)
                    d_stats = Helper.stats_from_list(d_times)
                    v_times = FormHelper.get_times(vis, end)
                    v_stats = Helper.stats_from_list(v_times)
                    res[d][r].append({
                        'name': dev,
                        'addr': addr,
                        'start': start,
                        'end': end,
                        # nano to seconds
                        'conn_first': (first_conn - start) / 10**9,
                        'conn_last':  (last_conn - start) / 10**9,
                        'net': net,
                        'conns': {'fail': f_stats, 'success': s_stats, 'server_rcv': server_rcv},
                        'dis': d_stats,
                        'vis': v_stats,
                        'list': {'conns': (fails, success, server_rcv), 'dis': d_times, 'vis': v_times}
                    })
        return res

    @staticmethod
    def get_conn_times(conn):
        fails = []
        success = []
        server_rcv = 0
        for (t1, t2, t3, t4, t4) in conn:
            if t4 == False:
                assert t3 > t2
                fails.append((t3 - t2) / 10**9)
            elif t2 != None:
                assert t3 > t2
                success.append((t3 - t2) / 10**9)
            elif t2 == None:
                server_rcv += 1

        return (
            fails,
            success,
            server_rcv
        )

    @staticmethod
    def get_times(arr, end_time):
        times = []
        for (t1, t2, t3) in arr:
            if (t2 < end_time):
                times.append((t2 - t1) / 10**9)
        return times

    def get_stats_from_file(file_path):
        m = {'addr': None, 'start_ts': None, 'first_conn_ts': None,
             'last_conn_ts': None, 'end_ts': None, 'is_server': False}
        dis, vis, server, conn, net = [], [], [], [], set()
        lost = set()

        def update_end(success, arr, ts):
            if success == True and m['start_ts'] != None:
                (t1, t2, t3) = arr[-1]
                if t2 is None:
                    arr[-1] = (t1, ts, t3)
        #print(file_path)

        def get_diff(s1, s2):
            c = 0
            for i in range(len(s1)):
                if s1[i] != s2[i]:
                    c += 1
            return c

        def line_process(line):
            ts, act = int(line[0]), line[1]
            if C.ACT_FORMATION == act:
                m['start_ts'] = ts
            elif C.ACT_PEER_ONLINE == act:
                m['addr'] = line[2]
            elif C.ACT_PEER_OFFLINE == act:
                m['off'] = True
                m['end_ts'] = ts
            elif C.ACT_DISCOVERY_ON == act:
                tp = line[2]
                if tp == 'WIFI':
                    return
                success = True if line[3] == 'true' else False
                if success == True:
                    dis.append((ts, None, success))
            elif C.ACT_DISCOVERY_OFF == act:
                tp = line[2]
                if tp == 'WIFI':
                    return
                success = True if line[3] == 'true' else False
                update_end(success, dis, ts)
            elif C.ACT_VISIBLE_ON == act:
                success = True if line[3] == 'true' else False
                vis.append((ts, None, success))
            elif C.ACT_VISIBLE_OFF == act:
                success = True if line[3] == 'true' else False
                update_end(success, vis, ts)
            elif C.ACT_SERVER_ON == act:
                success = True if line[3] == 'true' else False
                server.append((ts, None, success))
                m['is_server'] = success
            elif C.ACT_SERVER_OFF == act:
                success = True if line[3] == 'true' else False
                update_end(success, server, ts)
                if m['is_server'] and success == True:
                    m['is_server'] = False
            elif act == C.ACT_CONNECTION_ATTEMPT:
                dev = line[3]
                tp = line[2]
                ign = ["bc:ee:7b:8e:7f:30", "bc:ee:7b:8e:7f:34"]
                if tp == "WIFI" and dev in ign:
                    return
                conn.append((dev, ts, None, None, None))
            elif act == C.ACT_CONNECTION_NEW:
                dev = line[4]
                tp = line[2]
                success = True if line[3] == 'true' else False

                ign = ["bc:ee:7b:8e:7f:30", "bc:ee:7b:8e:7f:34"]
                if tp == "WIFI" and dev in ign:
                    return

                if success == True and len(lost) > 0:
                    for p in lost:
                        net.discard(p)
                    lost.clear()

                if success == False:
                    (t1, t2, t3, t4, t5) = conn[-1]
                    assert dev == t1
                    conn[-1] = (t1, t2, ts, t4, success)
                elif len(conn) > 0 and conn[-1][2] is None:
                    (t1, t2, t3, t4, t5) = conn[-1]
                    if tp == "WIFI":
                        d = get_diff(dev, t1)
                        assert d == 1 or d == 0
                        t1 = dev
                    else:
                        assert dev == t1
                    conn[-1] = (t1, t2, ts, t4, success)
                else:
                    conn.append((dev, None, ts, None, success))
            elif act == C.ACT_CONNECTION_LOST:
                dev = line[4]
                tp = line[2]
                success = True if line[3] == 'true' else False
                ign = ["bc:ee:7b:8e:7f:30", "bc:ee:7b:8e:7f:34"]
                if tp == "WIFI" and dev in ign:
                    return
                if success == True:
                    dev = line[4]
                    match = False
                    for i in range(len(conn) - 1, -1, -1):
                        (t1, t2, t3, t4, t5) = conn[i]
                        if t1 == dev:
                            assert t4 is None
                            conn[i] = (t1, t2, t3, ts, t5)
                            match = True
                            break
                    if match == False:
                        #print(conn)
                        raise Exception("Lost connection does not match")
                # print("conn lost")
            elif C.ACT_PEER_NEW == act:
                if m['first_conn_ts'] is None:
                    m['first_conn_ts'] = ts
                m['last_conn_ts'] = ts
                net.add(line[2])
            elif C.ACT_PEER_LOST == act:
                lost.add(line[2])
                pass
        Helper.read_lines(file_path, line_process)
        if dis and dis[-1][1] is None:
            dis.pop()
        if vis and vis[-1][1] is None:
            vis.pop()
        if server and server[-1][1] is None:
            server.pop()

        # validating discovery
        for (a, b, c) in dis:
            # print((a, b, c))
            assert a != None and b != None and b > a and c == True
        # validating visibility
        for (a, b, c) in vis:
            # print((a, b, c))
            assert a != None and b != None and b > a and c == True
        # validating server
        for (a, b, c) in server:
            # print( (a,b,c) )
            assert a != None and b != None and b > a and c == True
        # validating connections
        for (a, b, c, d, e) in conn:
            #print((a, b, c, d, e))
            assert a != None and c != None
            assert b == None or (b != None and c != None)
            assert d == None or (d != None and (b != None or c != None))
            assert e == True or (e == False and b != None and c != None)
        # validating network
        assert len(net) > 0
        # print("For Dev: %s Total: %d sec" %
        #      (m['addr'], (m['end_ts'] - m['start_ts']) / 10**9))
        return (m['addr'], m['start_ts'], m['end_ts'], m['first_conn_ts'],
                m['last_conn_ts'], dis, vis, server, conn, list(net))
