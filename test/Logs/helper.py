from consts import Consts as C
import os
import re
import time
from functools import reduce
from statistics import *
import math
import copy
from multiprocessing import Pool


class Helper:

    @staticmethod
    def get_cpu_idle_time(line):
        idle = int(line[6])
        iowait = int(line[7])
        return idle + iowait

    @staticmethod
    def get_cpu_non_idle_time(line):
        user = int(line[3])
        nice = int(line[4])
        system = int(line[5])
        irq = int(line[8])
        softirq = int(line[9])
        steal = int(line[10])
        return user + nice + system + irq + softirq + steal

    @staticmethod
    def get_cpu_total_time(line):
        guest = int(line[11])
        guest_nice = int(line[12])
        return Helper.get_cpu_idle_time() + Helper.get_cpu_non_idle_time() + guest + guest_nice

    @staticmethod
    def get_process_utime(line):
        utime = int(line[15])
        return utime

    @staticmethod
    def list_files(dir_path, full_path=False):
        lst = os.listdir(dir_path)
        return list(map(lambda x: "%s/%s" % (dir_path, x), lst)) if full_path else lst

    @staticmethod
    def list_files_recursive(path):
        if os.path.isfile(path):
            return path
        elif os.path.isdir(path):
            m = {}
            files = Helper.list_files(path)
            for f in files:
                new_path = "%s/%s" % (path, f)
                m[f] = Helper.list_files_recursive(new_path)
            return m
        else:
            raise Exception("Unknown type " + str(path))

    @staticmethod
    def get_navs(routing=None, formation=None, n_devs=None, dev=None, action=None):
        base_path = "/"
        navs = [(C.NAVS_ROUTING, routing, base_path)]
        if routing:
            p  = Helper.build_local_path(routing);
            if os.path.isdir(p):
                base_path += "%s/" % (routing)
                lst = Helper.list_files(p)
                lst.sort()
                lst = list(map(lambda x: x.lower(), lst))
                navs.append((lst, formation, base_path))

        if formation:
            base_path += "%s/" % (formation)
            folders = Helper.list_files(
                Helper.build_local_path(routing, formation))
            folders = sorted(folders, key=lambda x: int(x))
            folders.insert(0, "aggregate")
            navs.append((folders, n_devs, base_path))

        if n_devs and n_devs != 'aggregate':
            base_path += "%s/" % (n_devs)
            folders = []
            #print(routing)
            if routing != "formation":
                folders = Helper.list_files(
                    Helper.build_local_path(routing, formation, n_devs))
                folders = list(map(lambda x: x.split('.')[0], folders))
            folders.insert(0, "aggregate")
            navs.append((folders, dev, base_path))

        if n_devs == "aggregate":
            base_path += "%s/" % (n_devs)
            navs.append((C.NAVS_ACTIONS, dev, base_path))

        elif dev:
            base_path += "%s/" % (dev)
            acts = C.NAVS_ACTIONS
            if routing == "formation":
                acts = ['table_aggr'] + C.NAVS_ACTIONS
            navs.append((acts, action, base_path))
        return navs

    @staticmethod
    def build_local_path(routing=None, formation=None, n_devs=None, dev=None):
        def transform(value=None, ext=""):
            if value is None:
                return None
            if ext != "" and value.endswith(ext):
                return value
            return value.upper() + ext
        return Helper.build_path([transform(routing), transform(formation),
                                  transform(n_devs), transform(dev, ".log")])

    @staticmethod
    def build_path(lst, root="./", end=""):
        p = root
        lst = filter(lambda x: x != None, lst)
        return p if lst == [] else p + reduce(lambda x, y: "%s/%s" % (x, y), lst) + end

    @staticmethod
    def get_aggr_stats_map(path_dir, cache=None):
        devs_var = Helper.list_files(path_dir)
        m = dict(map(lambda x:
                     (int(x),
                      Helper.get_merged_device_stats(path_dir + "/" + x, cache)),
                     devs_var))
        return m

    @staticmethod
    def get_merged_device_stats(path_dir, cache=None):
        files = Helper.list_files(path_dir, True)
        merge_stats = []

        all_info = None
        for f in files:
            (key, _, _) = Helper.get_cache_keys(f)
            if all_info == None and cache != None and not cache.has_key(key):
                all_info = Helper.get_all_devices_info(path_dir)
            (cpu, mem, stats) = Helper.get_device_stats(f, cache, all_info)
            if merge_stats == []:
                for s in stats:
                    merge_stats.append(Helper.init_stats(s['n'], s['len'],
                                                         s['rate'], s['hops'], 0))
            for i in range(len(stats)):
                tags = set(C.TAGS_MAP.values())
                ka = ['pkts', 'traffic']
                kb = ['sent', 'rcv', 'route']
                combs = [(a, b, c) for a in ka for b in tags for c in kb]
                for(a, tag, b) in combs:
                    merge_stats[i][a][tag][b] += stats[i][a][tag][b]

                ka = ['latency']
                kb = ['full', 'mw']
                kb.extend(C.HOPS)
                combs = [(a,b) for a in ka for b in kb]
                for (a, b) in combs:
                    merge_stats[i][a][b].extend(stats[i][a][b])

        for i in range(len(merge_stats)):
            merge_stats[i] = Helper.close_stats(merge_stats[i])
        return merge_stats

    @staticmethod
    def get_cache_keys(path):
        key = path + "_dev_stats"
        key_cpu, key_mem = key + "_cpu", key + "_mem"
        return (key, key_cpu, key_mem)

    @staticmethod
    def get_all_devices_info(dir_path):
        files = Helper.list_files(dir_path, True)
        s_time = time.time()
        p = Pool(8)
        res, info_net, info_sent, info_rcv = [], {}, {}, {}
        for f in files:
            res.append(p.apply_async(Helper.load_packet_info, (f,)))
        for r in res:
            (addr, net, sent, rcv) = r.get()
            info_net[addr], info_sent[addr], info_rcv[addr] = net, sent, rcv
        p.close()
        print("Load Packet Info Time " + str(time.time() - s_time))
        return (info_net, info_sent, info_rcv)

    @staticmethod
    def get_device_stats(path, cache=None, all_info=None):
        print("Getting stats for device %s ... " %
              (path.split('/')[-1]), end="")

        def create_net_map(info_net):
            m = {}
            for (dev, conns) in info_net.items():
                for (k, v) in conns.items():
                    tech = v['tech']
                    val = m.get((dev, k), None)
                    if val != "WIFI_DIRECT":
                        m[dev, k], m[k, dev] = tech, tech
            return m
        (key, key_cpu, key_mem) = Helper.get_cache_keys(path)
        if cache:
            # if cache check cache and return if available
            stats = cache.get(key)
            cpu = cache.get(key_cpu)
            mem = cache.get(key_mem)
            if stats:
                print("On cache")
                return (cpu, mem, stats)
        print("Calculating...")
        if all_info == None:
            parent_path = re.sub(r'/\w+\.log$', '', path)
            all_info = Helper.get_all_devices_info(parent_path)
        (info_net, info_sent, info_rcv) = all_info

        # files = Helper.list_files(parent_path, True)
        # s_time = time.time()
        # p = Pool(8)
        # res, info_net, info_sent, info_rcv = [], {}, {}, {}
        # for f in files:
        #     res.append(p.apply_async(Helper.load_packet_info, (f,)))
        # for r in res:
        #     (addr, net, sent, rcv) = r.get()
        #     info_net[addr], info_sent[addr], info_rcv[addr] = net, sent, rcv
        # p.close()
        # print("Load Packet Info Time " + str(time.time() - s_time))
        (cpu, mem, stats) = Helper.load_stats(path, create_net_map(info_net),
                                              info_sent, info_rcv)

        if cache:
            # if cache updates entry
            cache.set(key, stats)
            cache.set(key_cpu, cpu)
            cache.set(key_mem, mem)
        return (cpu, mem, stats)

    @staticmethod
    def init_stats(pkt_n, pkt_len, pkt_rate, pkt_hops, start_ts):
        context = {'n': pkt_n,
                   'len': pkt_len,
                   'rate': pkt_rate,
                   'hops': pkt_hops,
                   'start_ts': start_ts,
                   'end_ts': 0,
                   'pkts': {}, 'traffic': {},
                   'latency': {'full': [], 'mw': []},
                   'latency_stats': {'full': {}, 'mw': {}}}
        for h in C.HOPS:
            context['latency'][h] = []
            context['latency_stats'][h] = {}
        for tag in set(C.TAGS_MAP.values()):
            context['pkts'][tag] = {'sent': 0, 'rcv': 0, 'route': 0}
            context['traffic'][tag] = {'sent': 0, 'rcv': 0, 'route': 0}
        return context

    @staticmethod
    def close_stats(cxt):
        new_cxt = copy.copy(cxt)
        lst = ['full', 'mw']
        lst.extend(C.HOPS)
        for l in lst:
            new_cxt['latency_stats'][l] = \
                Helper.stats_from_list(cxt['latency'][l])
        return new_cxt

    @staticmethod
    def load_stats(path, net_map, info_sent, info_rcv):
        m = {'addr': None, 'cxt': None,
             'prev_total': None, 'prev_idle': None, 'prev_utime': None}
        cpu_stats, mem_stats, stats = [], [], []

        def get_msg_overhead(src, dest, msg_id, rcv_ts):
            def get_id(src, dest, tags, msg_id):
                pkt_id = None
                for t in tags:
                    pkt_id = info_sent[src].get((dest, t, msg_id), None)
                    if pkt_id != None:
                        break
                return pkt_id

            def get_route(src, dest, pkt_id, hops=0, lst=[]):
                if (src == dest):
                    return (hops, lst)
                data = info_rcv[dest][src, pkt_id]
                from_ch = data['from_ch']
                tech = net_map[dest, from_ch]
                lst.insert(0, dest)
                hops += (2 if tech == "WIFI" else 1)
                return get_route(src, from_ch, pkt_id, hops, lst)


            def calc_overhead(src, dest, route, pkt_id):
                inf = info_sent[src][pkt_id]
                # send overhead on source device
                send_ov = inf['ts'] - inf['msg_ts']
                # route overhead
                route_ov = 0
                for i in range(len(route) - 1):
                    dev = route[i]
                    tmp_inf = info_rcv[dev][src, pkt_id]
                    assert tmp_inf['rcv_ts'] != None
                    route_ov += tmp_inf['rcv_ts'] - tmp_inf['ts']
                return send_ov + route_ov

            # request packet id that macthes 'msg_id'
            req_pkt_id = get_id(src, dest, C.MSG_REQ_TAG, msg_id)
            # response packet id that matches 'msg_id'
            ack_pkt_id = get_id(dest, src, C.MSG_ACK_TAG, msg_id)
            assert req_pkt_id != None
            assert ack_pkt_id != None

            (hops, route) = get_route(src, dest, req_pkt_id, 0, [])
            req_ov = calc_overhead(src, dest, route, req_pkt_id)
            req_ov += info_sent[dest][ack_pkt_id]['msg_ts'] - \
                info_rcv[dest][src, req_pkt_id]['ts']
            (_, route) = get_route(dest, src, ack_pkt_id, 0, [])
            ack_ov = calc_overhead(dest, src, route, ack_pkt_id)
            # overhead of the received ack
            ack_ov += rcv_ts - info_rcv[src][dest, ack_pkt_id]['ts']

            return (hops, (req_ov + ack_ov))

        def line_process(line):
            ts, act, addr = int(line[0]), line[1], m['addr']
            if C.ACT_PEER_ONLINE == act:
                m['addr'] = line[2]
            elif C.ACT_SYSTEM_STATS == act:
                # cpu stats
                utime = int(line[2])
                idle, non_idle = int(line[3]), int(line[4])
                total = idle + non_idle
                if m['prev_total']:
                    total_diff = total - m['prev_total']
                    idle_diff = idle - m['prev_idle']
                    utime_diff = utime - m['prev_utime']
                    total_cpu = (total_diff - idle_diff) / total_diff
                    proc_cpu = (utime_diff / total_diff)
                    cpu_stats.append((ts, total_cpu, proc_cpu))
                m['prev_total'] = total
                m['prev_idle'] = idle
                m['prev_utime'] = utime
                # memory stats
                total, rss, shared = int(line[5]), int(line[6]), int(line[7])
                mem_stats.append((ts, total, rss, shared))
            elif C.ACT_TEST_PACKET_BEGIN == act:
                n, size = int(line[2]), int(line[3]),
                rate, hops = int(line[4]), int(line[5])
                if m['cxt']:
                    stats.append(Helper.close_stats(m['cxt']))
                m['cxt'] = Helper.init_stats(n, size, rate, hops, ts)
            elif C.ACT_TEST_PACKET_END == act:
                m['cxt']['end_ts'] = ts
            elif C.ACT_TEST_PACKET_SEND == act:
                to_dev, msg_id = line[2], int(line[3])
                if msg_id in m:
                    raise Exception("Send map duplicate key " + str(msg_id))
                m[msg_id] = ts
            elif C.ACT_TEST_PACKET_RCV == act:
                pass
            elif C.ACT_TEST_PACKET_ACK == act:
                to_dev, msg_id = line[2], int(line[3])
                rtt = (ts - m[msg_id]) / 10**6  # nano to milliseconds
                m['cxt']['latency']['full'].append(rtt)
                # gets the message middleware overhead
                (hops, mw_ov) = get_msg_overhead(addr, to_dev, msg_id, ts)
                hops = hops if hops <= 4 else 4
                m['cxt']["latency"][str(hops)].append(rtt)
                m['cxt']['latency']['mw'].append(mw_ov / 10**6)
            elif m['cxt'] and C.ACT_PACKET_SEND == act:
                pkt_id = int(line[3])
                info = info_sent[addr][pkt_id]
                tag = C.TAGS_MAP[int(line[4])]
                m['cxt']['pkts'][tag]['sent'] += 1
                m['cxt']['pkts']['all']['sent'] += 1
                m['cxt']['traffic'][tag]['sent'] += info['len']
                m['cxt']['traffic']['all']['sent'] += info['len']
            elif m['cxt'] and C.ACT_DATAGRAM_ROUTE == act:
                src_dev, pkt_id = line[2], int(line[3])
                info = info_sent[src_dev][pkt_id]
                tag = C.TAGS_MAP[info['tag']]
                m['cxt']['pkts'][tag]['route'] += 1
                m['cxt']['pkts']['all']['route'] += 1
                m['cxt']['traffic'][tag]['route'] += info['len']
                m['cxt']['traffic']['all']['route'] += info['len']
            elif m['cxt'] and C.ACT_PACKET_RCV == act:
                src_dev, pkt_id = line[2], int(line[3])
                info = info_sent[src_dev][pkt_id]
                tag = C.TAGS_MAP[info['tag']]
                m['cxt']['pkts'][tag]['rcv'] += 1
                m['cxt']['pkts']['all']['rcv'] += 1
                m['cxt']['traffic'][tag]['rcv'] += info['len']
                m['cxt']['traffic']['all']['rcv'] += info['len']
            else:
                # do nothing
                pass

        Helper.read_lines(path, line_process)
        stats.append(Helper.close_stats(m['cxt']))
        return (cpu_stats, mem_stats, stats)

    @staticmethod
    def load_packet_info(dev_path):
        queue_sent, queue_rcv = [], []
        m = {'addr': None}
        net, sent, rcv = {}, {}, {}

        def line_process(line):
            ts, addr, act = int(line[0]), m['addr'], line[1]
            if C.ACT_PEER_ONLINE == act:
                m['addr'] = line[2]
            elif C.ACT_PEER_NEW == act:
                to, tech = line[2], line[3]
                net[to] = {'ts': ts, 'tech': tech}  # adds new entry 'net'
            elif C.ACT_PEER_LOST == act:
                to = line[2]
                net.pop(to, None)  # removes an entry from 'net'
            elif C.ACT_TEST_PACKET_SEND == act:
                msg_id = int(line[3])
                queue_sent.append((msg_id, ts))
            elif C.ACT_TEST_PACKET_RCV == act:
                dev_from, msg_id = line[2], int(line[3])
                queue_rcv.append((dev_from, msg_id, ts))
            elif C.ACT_PACKET_SEND == act:
                to = line[2]
                pkt_id, tag, pay_len = int(line[3]), int(line[4]), int(line[5])
                if pkt_id in sent and 'ts' in sent[pkt_id]:
                    raise Exception("Pkt id %d sent???" % (pkt_id))

                if tag in C.MSG_REQ_TAG:
                    (msg_id, msg_ts) = queue_sent.pop(0)
                    sent[to, tag, msg_id] = pkt_id
                elif tag in C.MSG_ACK_TAG:
                    (msg_id, msg_ts) = None, None
                    for i in range(len(queue_rcv)):
                        (dev_from, _, _) = queue_rcv[i]
                        if dev_from == to:
                            (_, msg_id, msg_ts) = queue_rcv.pop(i)
                            break
                    assert msg_id != None
                    sent[to, tag, msg_id] = pkt_id
                else:
                    msg_id, msg_ts = None, None

                sent.setdefault(pkt_id, {'to_ch': []})
                sent[pkt_id].update({
                    'ts': ts,
                    'to': to,
                    'tag': tag,
                    'len': pay_len,
                    'msg_id': msg_id,
                    'msg_ts': msg_ts
                })
            elif C.ACT_DATAGRAM_ROUTE == act:
                src_dev = line[2]
                pkt_id, full_len = int(line[3]), int(line[4])
                to_ch, ch_tech = line[5], line[6]
                # if packet is originated from the current device
                if src_dev == addr:
                    sent.setdefault(pkt_id, {'to_ch': []})
                    sent[pkt_id]['to_ch'].append((to_ch, ts))
                else:
                    rcv[src_dev, pkt_id]['to_ch'].append((to_ch, ts))
            elif C.ACT_PACKET_RCV == act:
                src_dev, pkt_id, from_ch = line[2], int(line[3]), line[4]
                if (src_dev, pkt_id) not in rcv:
                    rcv[src_dev, pkt_id] = {
                        'from_ch': from_ch,
                        'ts': ts,
                        'rcv_ts': None,  # the first index is the valid 'ts', the other are dropped 'ts'
                        'drop_ts': [],
                        'to_ch': [],
                    }
                # otherwise: # means that the packet will be dropped
            elif C.ACT_PACKET_VALID == act:
                src_dev, pkt_id = line[2], int(line[3])
                rcv[src_dev, pkt_id]['rcv_ts'] = ts
            elif C.ACT_PACKET_DROP == act:
                src_dev, pkt_id = line[2], int(line[3])
                rcv[src_dev, pkt_id]['drop_ts'].append(ts)

        Helper.read_lines(dev_path, line_process)
        if len(queue_sent) != 0:
            raise Exception("Queue sent not empty!! " + str(len(queue_sent)))
        if len(queue_rcv) != 0:
            raise Exception("Queue rcv not empty!!")
        return (m['addr'], net, sent, rcv)

    @staticmethod
    def read_lines(path, callback=None):
        with open(path) as f:
            if callback is not None:
                c = 0
                for line in f:
                    l = re.split(' |, ', line.strip())
                    callback(l)
                    c += 1
                return c
            else:
                lines = []
                for line in f:
                    l = re.split(' |, ', line.strip())
                    lines.append(l)
                return lines

    @staticmethod
    def stats_from_list(lst, r_zeros = False):
        cnt, avg, std, q1, q2, q3, mi, ma, conf = 0, 0, 0, 0, 0, 0, 0, 0, 0

        if r_zeros == True:
            lst = list( filter(lambda x: x > 0, lst) )

        if(len(lst) == 1):
            cnt = 1
            avg, std = lst[0], 0
            mi, ma = lst[0], lst[0]

        if(len(lst) > 1):
            lst.sort()
            cnt = len(lst)
            avg, std = mean(lst), stdev(lst)
            mi, ma = min(lst), max(lst)

            q2 = median(lst)

            mid = len(lst) // 2
            if(len(lst) % 2 == 0):
                q1 = median(lst[:mid])
                q3 = median(lst[mid:])
            else:
                q1 = median(lst[:mid])
                q3 = median(lst[(mid + 1):])
            conf = C.CONF_95 * (std / math.sqrt(cnt))

        iqr = q3 - q1
        lw_lim = q1 - 1.5*iqr
        hg_lim = q3 + 1.5*iqr

        lw_wsk = None
        up_wsk = None
        for v in lst:
            if lw_wsk is None and v >= lw_lim:
                lw_wsk = v
            if v > hg_lim:
                break
            up_wsk = v

        out = list( filter( lambda x: x > hg_lim or x < lw_lim, lst) )
        return {'cnt': cnt, 'avg': avg, 'std': std, 'conf': conf, 'q1': q1,
                'q2': q2, 'q3': q3, 'min': mi, 'max': ma, 'iqr':iqr,
                'lw_wsk':lw_wsk, 'up_wsk':up_wsk, 'out': out}
