
class Consts:
    LOGS_FOLDER = "./Logs/"
    PACKAGE = "hyrax.dcc.fc.up.pt.hyraxnetworklayerapp"
    ACTIVITY = PACKAGE + "/.MainActivity"
    REMOTE_PATH = "/storage/emulated/0/Download/"

    # intents
    NETWORK_START = "android.hyrax.network_start"
    NETWORK_STOP = "android.hyrax.network_stop"
    FORMATION_ENABLE = "android.hyrax.formation_enable"
    FORMATION_DISABLE = "android.hyrax.formation_disable"
    FORMATION_PAUSE = "android.hyrax.formation_pause"
    FORMATION_RESUME = "android.hyrax.formation_resume"
    SEND_PACKETS = "android.hyrax.send_packets"

    # wifi intents
    WIFI_HW_ENABLE = "android.hyrax.wifi_hw_enable"
    WIFI_HW_DISABLE = "android.hyrax.wifi_hw_disable"
    WIFI_NET_CONNECT = "android.hyrax.wifi_net_connect"
    WIFI_NET_DISCONNECT = "android.hyrax.wifi_net_disconnect"

    # wifi args
    ARG_SSID = "SSID"
    ARG_PASS = "PASS"
    ARG_TO_REMOVE = "TO_REMOVE"

    # formation args
    ARG_DIS_LM_LW = "DIS_LM_LW" # min discovery time. INTEGER
    ARG_DIS_LM_HG = "DIS_LM_HG" # max discovery time. INTEGER
    ARG_VIS_LM_LW = "VIS_LM_LW" # min visibility time. INTEGER
    ARG_VIS_LM_HG = "VIS_LM_HG" # max visibility time. INTEGER
    ARG_CONN_LM_LW = "CONN_LM_LW" # min connections number. INTEGER
    ARG_CONN_LM_HG = "CONN_LM_HG" # max connections number. INTEGER
    ARG_ACCEPT_PROB = "ACCEPT_PROB" # accept connections probability. FLOAT
    ARG_IDLE_TIME = "IDLE_TIME" # master idle time. INTEGER
    ARG_ENABLE_HW = "ENABLE_HW" # to enable hardware at startup. BOOLEAN

    # args
    ARG_TYPE = "TYPE"
    ARG_LOG_FILE_NAME = "LOG_FILE_NAME"
    ARG_LISTEN_PORT = "LISTEN_PORT"
    ARG_PKT_NUMBER = "PKT_NUMBER"
    ARG_PKT_LENGTH = "PKT_LENGTH"
    ARG_PKT_RATE = "PKT_RATE"
    ARG_NET_HOPS = "NET_HOPS"
    ARG_TO_STOP = "APP_STOP"
    ARG_DELAY = "DELAY";

    # values
    # routing algorithms
    ROUTING_FLOOD = "FLOOD"
    ROUTING_FLOOD_CONTROL = "FLOOD_CONTROL"
    # formation algoritmhs
    FORMATION_BLUETOOTH_STAR = "BLUETOOTH_STAR"
    FORMATION_BLUETOOTH_MESH_RANDOM = "BLUETOOTH_MESH_RANDOM"
    FORMATION_WIFI_DIRECT_STAR = "WIFI_DIRECT_STAR"
    FORMATION_WIFI_DIRECT_LEGACY_STAR = "WIFI_DIRECT_LEGACY_STAR"
    FORMATION_WIFI_DIRECT_LEGACY_STAR_2 = "WIFI_DIRECT_LEGACY_STAR_2"
    FORMATION_WIFI_MESH_RANDOM = "WIFI_MESH_RANDOM"

    # barrier file keys
    BARRIER_FILE = REMOTE_PATH + "network.barrier"
    BARRIER_DONE = "done"
