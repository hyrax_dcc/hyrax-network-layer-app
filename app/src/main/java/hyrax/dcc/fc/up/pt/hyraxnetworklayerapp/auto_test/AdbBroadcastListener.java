/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.auto_test;

import android.support.annotation.NonNull;

import java.util.Map;

/**
 * Listen for adb commands.
 */
public interface AdbBroadcastListener {

    /**
     * Starts the network with a specific routing algorithm.
     *
     * @param routingType routing algorithm type
     * @param logFileName log file name
     * @param listenPort  network viewer listen port, or 0 if not defined
     */
    void onNetworkStart(@NonNull String routingType, @NonNull String logFileName, int listenPort);

    /**
     * Stops the network.
     *
     * @param stopApp <tt>true</tt> to stop the app <tt>false</tt> otherwise
     * @param delay   stop app delay
     */
    void onNetworkStop(boolean stopApp, long delay);

    /**
     * Enables a network formation algorithm.
     *
     * @param formationType network formation type
     * @param args          a map with arguments
     */
    void onFormationEnable(@NonNull String formationType, @NonNull Map<String, String> args);

    /**
     * Disables a network formation algorithm.
     *
     * @param formationType network formation type
     */
    void onFormationDisable(@NonNull String formationType);

    /**
     * Pauses all network formation algorithms.
     */
    void onFormationPause();

    /**
     * Resumes all network formation algorithms.
     */
    void onFormationResume();

    /**
     * Sends packets to random remote devices using a poisson distribution.
     * <p/>
     * The poisson distribution is used to know when to send the next packet
     *
     * @param nPack     number of packets to send
     * @param pktLength the length of each packet
     * @param pktRate   the rate of packets per second
     * @param nHops     the maximum number of hops to find a peer
     */
    void onSendPacket(int nPack, int pktLength, int pktRate, int nHops);
}
