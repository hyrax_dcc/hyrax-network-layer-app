/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package hyrax.dcc.fc.up.pt.hyraxnetworklayerapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.hyrax.network.Address;
import org.hyrax.network.Formation;
import org.hyrax.network.FormationProperties;
import org.hyrax.network.Network;
import org.hyrax.network.NetworkPacketListener;
import org.hyrax.network.NetworkPeerListener;
import org.hyrax.network.NetworkService;
import org.hyrax.network.Routing;
import org.hyrax.network.RoutingProperties;
import org.hyrax.network.formation.LibFormation;
import org.hyrax.network.misc.NetLog;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.auto_test.AdbBroadcast;
import hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.auto_test.AdbBroadcastListener;
import hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.helpers.ActivityComm;
import hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.helpers.ActivityHelper;
import hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.helpers.HandlerHelper;
import hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.helpers.NetworkUiActionsListener;
import hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.helpers.PermissionManager;

public class MainActivity extends AppCompatActivity implements NetworkUiActionsListener,
        NetworkPeerListener, NetworkPacketListener, AdbBroadcastListener {
    private static final int STRING_MSG_TAG = 130;
    private static final int PEER_FIND_TAG = 131;
    private static final int PACKET_REQ_TAG = 132;
    private static final int PACKET_ACK_TAG = 133;

    private static final String TAG = "NetworkActivity";
    // activity helper
    private HandlerHelper handlerHelper;
    // network object
    @Nullable
    private Network network;

    private ArrayList<String> peersFound;

    private AdbBroadcast adbBroadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handlerHelper = new HandlerHelper(this);
        peersFound = new ArrayList<>();
        adbBroadcast = new AdbBroadcast(this);
        adbBroadcast.enableWifiSetup(this);

        Log.w(TAG, "onCreate");
        NetLog.enable();
        NetLog.addNetLogListener((level, logEntry) -> {
            String msg = String.format(Locale.ENGLISH, "%s: %s", logEntry.tag, logEntry.message);
            switch (level) {
                case INFO:
                    handlerHelper.info(msg);
                    break;
                case DEBUG:
                    handlerHelper.debug(msg);
                    break;
                case WARNING:
                    handlerHelper.warning(msg);
                    break;
                case ERROR:
                    handlerHelper.error(msg);
                    break;
                default:
                    throw new RuntimeException("Undefined level " + level);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.w(TAG, "OnStart Called");

        // register adb command broadcast
        registerReceiver(adbBroadcast, adbBroadcast.getIntentFilters());

        handlerHelper.getPermissionManager().requestPermission(
                PermissionManager.PERMISSION_WRITE_STORAGE,
                granted -> {
                    if (!granted) {
                        handlerHelper.warning("Write Permission not granted");
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        handlerHelper.getPermissionManager().onPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.w(TAG, "onStop");
        NetLog.disable();
        if (network != null) {
            NetworkService.shutdown();
            network = null;
        }
        //closes the properties manager out stream
        adbBroadcast.propertiesManager.close();
        // removes adb command broadcast
        unregisterReceiver(adbBroadcast);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w(TAG, "onDestroy");
    }

    @Override
    public void onActionStop(MenuItem menuItem) {
        finishAndRemoveTask();
    }

    @Override
    public void onActionNetwork(View view) {
        if (!NetworkService.isRunning()) {
            handlerHelper.getPermissionManager().requestPermission(
                    PermissionManager.PERMISSION_LOCATION,
                    granted -> {
                        if (!granted) {
                            handlerHelper.error("Location permission denied");
                            handlerHelper.toastLong("Location permissions are required");
                            return;
                        }
                        // boots the network layer
                        String[] options = ActivityHelper.getRoutingTypes();
                        ActivityHelper.showOptionsDialog(this,
                                "Select the routing algorithm",
                                options,
                                opt -> {
                                    Routing.Type type = Routing.Type.valueOf(options[opt]);
                                    network = NetworkService.boot(this, type);
                                    network.addNetworkListener(this);
                                    network.addPacketListener(this);

                                    handlerHelper.toggleButton(
                                            ActivityComm.Button.NETWORK,
                                            ActivityComm.Button.State.ON, 0);
                                    handlerHelper.success("Network initiated with routing " + type);
                                });
                    });
        } else {
            // shutdown the network layer
            NetworkService.shutdown();
            network = null;
            handlerHelper.toggleButton(
                    ActivityComm.Button.NETWORK, ActivityComm.Button.State.OFF, 0);
            handlerHelper.warning("Network disabled");
        }
    }

    @Override
    public void onActionFormation(View view) {
        if (!NetworkService.isRunning()) {
            handlerHelper.toastShort("Network not running");
            return;
        }
        assert network != null;

        String[] types = ActivityHelper.getFormationTypes();
        String[] options = new String[types.length + 2];
        for (int i = 0; i < types.length; i++) {
            Formation.Type type = Formation.Type.valueOf(types[i]);
            String extra = (network.isTopologyEnabled(type)) ? " (Enabled)" : "";
            options[i] = types[i] + extra;
        }
        options[options.length - 2] = "Pause Formation";
        options[options.length - 1] = "Resume Formation";
        ActivityHelper.showOptionsDialog(this,
                "Select the network topology Enable/Disable",
                options,
                opt -> {
                    if (opt == options.length - 2) {
                        for (Formation f : network.topologies)
                            f.pause();
                    } else if (opt == options.length - 1) {
                        for (Formation f : network.topologies)
                            f.resume();
                    } else {
                        Formation.Type type = Formation.Type.valueOf(types[opt]);
                        if (network.isTopologyEnabled(type)) {
                            network.disableFormation(type);
                            if (!network.isOnline())
                                handlerHelper.toggleButton(
                                        ActivityComm.Button.FORMATION,
                                        ActivityComm.Button.State.OFF, 0
                                );
                        } else {
                            network.enableFormation(type);
                            handlerHelper.toggleButton(
                                    ActivityComm.Button.FORMATION,
                                    ActivityComm.Button.State.ON, 0
                            );
                        }
                    }
                });
    }

    @Override
    public void onActionUnicast(View view) {
        if (!NetworkService.isRunning()) {
            handlerHelper.toastShort("Network not running");
            return;
        }
        assert network != null;

        String[] actions = {"Find Peers", "Send Message"};
        ActivityHelper.showOptionsDialog(this, "Select Action", actions,
                act -> {
                    switch (act) {
                        case 0:
                            peersFound.clear();
                            peersFound.add(0, "Select a peer ...");
                            // sends to every one a peer find message
                            network.sendToAll(new byte[]{1}, PEER_FIND_TAG);
                            break;
                        case 1:
                            if (peersFound.size() == 0) {
                                handlerHelper.toastShort("No peers found. Search first");
                                return;
                            }

                            ActivityHelper.showDialogWithView(this, R.layout.message_layout,
                                    "Enter Message",
                                    readyView -> {
                                        // prepares the spinner adapter
                                        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                                                this,
                                                R.layout.support_simple_spinner_dropdown_item,
                                                peersFound);
                                        Spinner spinner = readyView.findViewById(R.id.peers);
                                        spinner.setVisibility(View.VISIBLE);
                                        spinner.setAdapter(adapter);

                                    },
                                    dialogView -> {
                                        EditText editMsg = dialogView.findViewById(R.id.message);
                                        EditText editHops = dialogView.findViewById(R.id.hops);
                                        Spinner spinner = dialogView.findViewById(R.id.peers);

                                        byte[] msg = editMsg.getText().toString().getBytes();
                                        int hops = Integer.valueOf(editHops.getText().toString());
                                        Object adr = spinner.getSelectedItem();
                                        if (hops <= 0) {
                                            handlerHelper.error("Number of message hops must be greater than 0");
                                            return;
                                        }
                                        if (adr == null || adr.equals("Select a peer ...")) {
                                            handlerHelper.error("No address selected");
                                            return;
                                        }

                                        RoutingProperties properties = RoutingProperties
                                                .newBuilder(STRING_MSG_TAG)
                                                .setMaxNumberOfHops(hops)
                                                .build();
                                        Address dest = Address.AddressFromUUID(UUID.fromString((String) adr));
                                        network.send(msg, dest, properties);
                                    });
                            break;
                        default:
                            throw new RuntimeException("Undefined action " + act);
                    }
                });
    }

    @Override
    public void onActionBroadcast(View view) {
        if (!NetworkService.isRunning()) {
            handlerHelper.toastShort("Network not running");
            return;
        }
        assert network != null;

        ActivityHelper.showDialogWithView(this, R.layout.message_layout,
                "Broadcast Message", dialogView -> {
                    EditText editMsg = dialogView.findViewById(R.id.message);
                    EditText editHops = dialogView.findViewById(R.id.hops);

                    byte[] msg = editMsg.getText().toString().getBytes();
                    int hops = Integer.valueOf(editHops.getText().toString());

                    if (hops <= 0) {
                        handlerHelper.error("Number of message hops must be greater than 0");
                        return;
                    }

                    RoutingProperties properties = RoutingProperties
                            .newBuilder(STRING_MSG_TAG)
                            .setMaxNumberOfHops(hops)
                            .build();

                    network.sendToAll(msg, properties);
                });
    }

    @Override
    public void onActionUpload(View view) {

    }

    int i = 0;

    @Override
    public void onNewPacket(@NonNull byte[] bytes, int len, @NonNull Address srcAddress, int tag) {
        assert network != null;
        switch (tag) {
            case STRING_MSG_TAG:
                handlerHelper.debug(String.format(Locale.ENGLISH,
                        "A new string tag packet from %s msg: %s",
                        srcAddress.toString(), new String(bytes)));
                break;

            case PEER_FIND_TAG:
                if (!NetworkService.getLocalAddress().equals(srcAddress)) {
                    // means that receives a ping find request - then sends a response to the source address
                    if (len > 0) {
                        //handlerHelper.debug("Peer find request from " + srcAddress.toString());
                        network.send(new byte[]{}, srcAddress, PEER_FIND_TAG);
                    } else {
                        handlerHelper.info("Resp from " + srcAddress.toString());
                        peersFound.add(srcAddress.toString());
                    }
                }

                break;
            case PACKET_REQ_TAG:
                // wraps the header
                int req_id = ByteBuffer.wrap(bytes, 0, 4).getInt();
                ByteBuffer toSend = ByteBuffer.allocate(4);
                toSend.putInt(req_id);
                // logs test packet receive
                NetLog.logTestPacketRcv(srcAddress.toString(), req_id);

                // send ack
                network.send(toSend.array(), srcAddress, PACKET_ACK_TAG);
                break;

            case PACKET_ACK_TAG:
                //ByteBuffer buffer_ack = ByteBuffer.wrap(bytes, 0, 4);
                ByteBuffer buffer_ack = ByteBuffer.wrap(bytes, 0, 4);
                int ack_id = buffer_ack.getInt();
                // logs test packet ack
                NetLog.logTestPacketAck(srcAddress.toString(), ack_id);
                //String msg = String.format(Locale.ENGLISH, "%s ACK %d - %d", srcAddress.toString(), ack_id, i++);
                //handlerHelper.debug(msg);
                break;

            default:
                handlerHelper.debug(String.format(Locale.ENGLISH, "A new packet from %s tag: %d size: %d",
                        srcAddress.toString(), tag, bytes.length));
        }
    }

    @Override
    public void onPeerJoin(@NonNull Address address) {
        handlerHelper.info("A peer have joined the network " + address.toString());
    }

    @Override
    public void onPeerLeave(@NonNull Address address) {
        handlerHelper.warning("A peer left the network " + address.toString());
    }

    int pktMsgId = 0;

    @Override
    public void onNetworkStart(@NonNull String routingType, @NonNull String logFileName, int listenPort) {
        if (listenPort > 0)
            NetLog.enableLogHandler(this, AdbBroadcast.BASE_PATH + logFileName, listenPort);
        pktMsgId = 0;
        network = NetworkService.boot(this, getRoutingTypeFromString(routingType));
        network.addNetworkListener(this);
        network.addPacketListener(this);
        handlerHelper.success("Network started with routing " + routingType);
    }

    @Override
    public void onNetworkStop(boolean stopApp, long delay) {
        if (network != null) {
            NetworkService.shutdown();

            if (stopApp) {
                handlerHelper.postDelayed(() -> finishAndRemoveTask(), delay);
            }
        }
    }

    @Override
    public void onFormationEnable(@NonNull String formationType, @NonNull Map<String, String> args) {
        if (network != null) {
            if (args.size() == 0) {
                network.enableFormation(getFormationTypeFromString(formationType));
            } else {
                Formation formation = LibFormation.factory(getFormationTypeFromString(formationType));
                FormationProperties defProps = formation.getDefaultFormationProperties();
                FormationProperties.Builder newProps = FormationProperties.newBuilder(defProps);

                int disMin = defProps.getLimitsTimeDiscovery().min;
                int disMax = defProps.getLimitsTimeDiscovery().max;
                int visMin = defProps.getLimitsTimeVisibility().min;
                int visMax = defProps.getLimitsTimeVisibility().max;
                int connMin = defProps.getLimitsConnections().min;
                int connMax = defProps.getLimitsConnections().max;

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (Map.Entry<String, String> entry : args.entrySet()) {
                    String key = entry.getKey();
                    String val = entry.getValue();
                    NetLog.d(TAG, String.format(Locale.ENGLISH, "Setting key %s with val: %s",
                            key, val));
                    switch (key) {
                        case AdbBroadcast.ARG_DIS_LM_LW:
                            disMin = Integer.valueOf(val);
                            break;
                        case AdbBroadcast.ARG_DIS_LM_HG:
                            disMax = Integer.valueOf(val);
                            break;
                        case AdbBroadcast.ARG_VIS_LM_LW:
                            visMin = Integer.valueOf(val);
                            break;
                        case AdbBroadcast.ARG_VIS_LM_HG:
                            visMax = Integer.valueOf(val);
                            break;
                        case AdbBroadcast.ARG_CONN_LM_LW:
                            connMin = Integer.valueOf(val);
                            break;
                        case AdbBroadcast.ARG_CONN_LM_HG:
                            connMax = Integer.valueOf(val);
                            break;
                        case AdbBroadcast.ARG_ACCEPT_PROB:
                            newProps.setAcceptProbability((double) Float.valueOf(val));
                            break;
                        case AdbBroadcast.ARG_IDLE_TIME:
                            newProps.setIdleTime(Integer.valueOf(val));
                            break;
                        case AdbBroadcast.ARG_ENABLE_HW:
                            newProps.setEnableHardwareOnStart(Boolean.valueOf(val));
                            break;
                        default:
                            throw new RuntimeException("No key defined " + key);
                    }
                }
                newProps.setDiscoveryTimeLimits(disMin, disMax)
                        .setVisibilityTimeLimits(visMin, visMax)
                        .setConnectionLimits(connMin, connMax);
                network.enableFormation(formation, newProps.build());
            }
            NetLog.logFormation(formationType, "enabled");
        }
    }

    @Override
    public void onFormationDisable(@NonNull String formationType) {
        if (network != null) {
            network.disableFormation(getFormationTypeFromString(formationType));
            NetLog.logFormation(formationType, "disabled");
        }
    }

    @Override
    public void onFormationPause() {
        if (network != null) {
            for (Formation f : network.topologies) {
                f.pause();
                NetLog.logFormation(f.getType().name(), "paused");
            }
        }
    }

    @Override
    public void onFormationResume() {
        if (network != null) {
            for (Formation f : network.topologies) {
                f.resume();
                NetLog.logFormation(f.getType().name(), "resumed");
            }
        }
    }

    @Override
    public void onSendPacket(final int nPack, int pktLength, int pktRate, int nHops) {
        if (network != null) {
            // log test begin
            NetLog.logTestPacketBegin(nPack, pktLength, pktRate, nHops);
            String key = String.format(Locale.ENGLISH, "Send Packet Begin: (%d, %d, %d, %d)",
                    nPack, pktLength, pktRate, nHops);
            handlerHelper.info(key);
            // searches for peers
            findPeers(nHops);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        int attempts = 19;
                        while (peersFound.size() == 0 && attempts > 0) {
                            if (attempts % 4 == 0)
                                findPeers(nHops);
                            Thread.sleep(500);
                            attempts--;
                        }

                        Random random = new Random();
                        // generates a random message - once
                        byte[] msg = new byte[pktLength];
                        random.nextBytes(msg);
                        RoutingProperties properties = RoutingProperties
                                .newBuilder(PACKET_REQ_TAG)
                                .setMaxNumberOfHops(nHops)
                                .build();
                        ByteBuffer buff = ByteBuffer.allocate(4);
                        if (peersFound.size() > 0) {
                            int n = nPack;
                            double rate = pktRate / 1000.0;
                            while (n > 0) {
                                Thread.sleep((long) timeUntilNextPacket(random, rate));
                                // choose randomly a peer to send the packet
                                String rmtDev = peersFound.get(random.nextInt(peersFound.size()));
                                Address dest = Address.AddressFromUUID(UUID.fromString(rmtDev));
                                //byte[] bId = ByteBuffer.allocate(4).putInt(pktMsgId).array();

                                System.arraycopy(buff.putInt(pktMsgId).array(), 0, msg, 0, 4);

                                // logs test packet send
                                NetLog.logTestPacketSend(rmtDev, pktMsgId);
                                // send message to destination
                                network.send(msg, dest, properties);
                                //ByteBuffer buffer_req = ByteBuffer.wrap(msg, 0, 4);
                                Log.w("SENT", String.format("ID: %d", pktMsgId));
                                n--;
                                pktMsgId++;
                                buff.clear();
                            }
                        } else {
                            NetLog.e("PACKETS", "No packets sent");
                            handlerHelper.post(() -> {
                                String[] options = {key};
                                ActivityHelper.showOptionsDialog(MainActivity.this, "ERROR", options, (w) -> {
                                });
                            });
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        NetLog.e("PktThread", e.getMessage());
                    }
                    // logs test end
                    NetLog.logTestPacketEnd();
                    handlerHelper.info("Send Packet END");
                    // writes done to barrier file
                    String key = String.format(Locale.ENGLISH, "done_%d_%d_%d", pktLength, pktRate, nHops);
                    adbBroadcast.propertiesManager.setProperties(key, Boolean.toString(true));
                }

                /**
                 * Returns the time to send the next packet.
                 * @param random random object
                 * @param rate packet send rate interval
                 * @return the wait time
                 */
                private double timeUntilNextPacket(Random random, double rate) {
                    return -Math.log(1.0 - random.nextDouble()) / rate;
                }
            }).start();
        }
    }

    private void findPeers(int nHops) {
        peersFound.clear();
        if (network != null)
            network.sendToAll(new byte[]{1},
                    RoutingProperties.newBuilder(PEER_FIND_TAG)
                            .setMaxNumberOfHops(nHops)
                            .build());
    }

    /**
     * Returns a routing type object form a string.
     *
     * @param type string type
     * @return routing type object
     */
    @NonNull
    private Routing.Type getRoutingTypeFromString(@NonNull String type) {
        for (Routing.Type r : Routing.Type.values()) {
            if (type.equals(r.toString()))
                return r;
        }
        throw new RuntimeException("Undefined routing type " + type);
    }

    /**
     * Returns a formation type object form a string.
     *
     * @param type string type
     * @return formation type object
     */
    @NonNull
    private Formation.Type getFormationTypeFromString(@NonNull String type) {
        for (Formation.Type t : Formation.Type.values()) {
            if (type.equals(t.toString()))
                return t;
        }
        throw new RuntimeException("Undefined formation type " + type);
    }
}

