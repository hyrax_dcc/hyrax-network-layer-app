/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.auto_test;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.network.misc.NetLog;

import java.util.List;
import java.util.Locale;

public class WifiSetup {
    private final String TAG = "WifiSetup";
    private final WifiManager wifiManager;

    /**
     * Constructor.
     *
     * @param context application context
     */
    WifiSetup(@NonNull Context context) {
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    /**
     * Enables the wifi hardware.
     */
    void enable() {
        if (!wifiManager.isWifiEnabled())
            wifiManager.setWifiEnabled(true);
    }

    /**
     * Disables the wifi hardware.
     */
    void disable() {
        if (wifiManager.isWifiEnabled())
            wifiManager.setWifiEnabled(false);
    }

    /**
     * Connects to a Wifi network already configured.
     *
     * @param ssid network ssid
     */
    void connect(@NonNull String ssid) {
        int id = getNetworkId(ssid);
        if (id == -1 || !connectNetwork(id))
            Log.e(TAG, String.format(Locale.ENGLISH,
                    "Connect network %s ID: %d) failure", ssid, id));
    }

    /**
     * Connects to a Wifi network with a password.
     * If network already configured just connects otherwise add a network.
     *
     * @param ssid     network ssid
     * @param password network password
     */
    void connect(@NonNull String ssid, @NonNull String password) {
        int id = getNetworkId(ssid);
        if (id == -1) {
            WifiConfiguration conf = new WifiConfiguration();
            conf.SSID = "\"" + ssid + "\"";
            conf.preSharedKey = "\"" + password + "\"";
            id = wifiManager.addNetwork(conf);
            wifiManager.saveConfiguration();
        }
        if (id == -1 && !connectNetwork(id))
            Log.e(TAG, String.format(Locale.ENGLISH,
                    "Connect network %s ID: %d) failure", ssid, id));
    }

    /**
     * Connects to a wifi network given a network id.
     *
     * @param networkId network id
     * @return @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    private boolean connectNetwork(int networkId) {
       return wifiManager.enableNetwork(networkId, true);
    }

    /**
     * Disconnects the device from a network.
     *
     * @param ssid          network ssid
     * @param removeNetwork <tt>true</tt> to remove the network
     */
    void disconnect(@NonNull String ssid, boolean removeNetwork) {
        int id = getNetworkId(ssid);
        boolean disabled = (removeNetwork)
                ? id != -1 && removeNetwork(id)
                : id != -1 && disableNetwork(id);
        if (!disabled)
            Log.e(TAG, String.format(Locale.ENGLISH,
                    "Disconnect network %s ID: %d) failure", ssid, id));
    }

    /**
     * Disables (disconnects) a network given a network id.
     *
     * @param networkId network id
     * @return @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    private boolean disableNetwork(int networkId) {
        return wifiManager.disableNetwork(networkId) && wifiManager.saveConfiguration();
    }

    /**
     * Disables and removes a network from configuration, given a network id.
     *
     * @param networkId network id
     * @return @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    private boolean removeNetwork(int networkId) {
        return wifiManager.disableNetwork(networkId) && wifiManager.removeNetwork(networkId)
                && wifiManager.saveConfiguration();
    }

    /**
     * Return a network id given a ssid.
     *
     * @param ssid network ssid
     * @return network id
     */
    private int getNetworkId(@NonNull String ssid) {
        String configSSID = "\"" + ssid + "\"";
        List<WifiConfiguration> confNetworks = wifiManager.getConfiguredNetworks();
        if (confNetworks != null) {
            for (WifiConfiguration network : confNetworks) {
                if (network.SSID.equals(configSSID)) {
                    return network.networkId;
                }
            }
        }
        return -1;
    }
}
