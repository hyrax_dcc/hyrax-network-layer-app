/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.helpers;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.ColorRes;
import android.support.annotation.IdRes;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.R;

/**
 * This handler acts as a bridge between the link layer and the UI (main thread).
 * <br/>
 * Essentially it changes the UI depending on the type of events that receives.
 */
public class HandlerHelper extends Handler implements ActivityComm {
    private final Activity activity;
    private final PermissionManager permissionManager;

    /**
     * Constructor.
     *
     * @param activity application activity
     */
    public HandlerHelper(@NonNull Activity activity) {
        super(activity.getMainLooper());
        this.activity = activity;
        this.permissionManager = new PermissionManager(activity);
    }

    @Override
    public void handleMessage(Message msg) {
        //handle incoming messages
    }

    @Override
    public void message(@NonNull String message, @NonNull Color color) {
        post(() -> {
            logMessage(activity.findViewById(R.id.debug), message, color);
            ((ScrollView) activity.findViewById(R.id.scroll_view)).fullScroll(View.FOCUS_DOWN);
        });
    }

    /**
     * Logs a messages to a text view.
     *
     * @param view    text view object
     * @param message string message
     * @param color   message color
     */
    @MainThread
    private void logMessage(@NonNull TextView view, @NonNull String message, @NonNull Color color) {
        String msg = String.format("<font color=\"%s\">%s</font><br/>", color.hex, message);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            view.append(Html.fromHtml(msg, Html.FROM_HTML_MODE_LEGACY));
        else
            view.append(Html.fromHtml(msg));
    }

    @Override
    public void toastLong(@NonNull String message) {
        post(() -> Toast.makeText(activity, message, Toast.LENGTH_LONG).show());
    }

    @Override
    public void toastShort(@NonNull String message) {
        post(() -> Toast.makeText(activity, message, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void toggleButton(@NonNull Button button, @NonNull Button.State state, int count) {
        int btnId;
        switch (button) {
            case NETWORK:
                btnId = R.id.network;
                break;
            case FORMATION:
                btnId = R.id.formation;
                break;
            default:
                throw new RuntimeException("Undefined button " + button);
        }
        switch (state) {
            case ON:
                setButtonStatus(btnId, R.color.teal_50);
                break;
            case OFF:
                setButtonStatus(btnId, R.color.red_100);
                break;
            default:
                throw new RuntimeException("Undefined button state " + state);
        }
    }

    /**
     * Sets the button status.
     *
     * @param id    button resource id
     * @param color new color
     */
    private void setButtonStatus(@IdRes int id, @ColorRes int color) {
        post(() -> {
            //Gets button object
            android.widget.ImageButton btn = activity.findViewById(id);
            btn.setBackgroundColor(ContextCompat.getColor(activity, color));
        });
    }

    /**
     * Returns the permission manager object.
     *
     * @return permission manager object
     */
    @NonNull
    public PermissionManager getPermissionManager() {
        return permissionManager;
    }
}
