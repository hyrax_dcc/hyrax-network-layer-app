/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.auto_test;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

/**
 * Class responsible for listen intents from command line (adb).
 */
public class AdbBroadcast extends BroadcastReceiver {
    public static final String BASE_PATH = "/storage/emulated/0/Download/";
    //barrier file name
    private static final String BARRIER_FILE = BASE_PATH + "network.barrier";

    //intent arguments
    private static final String ARG_TYPE = "TYPE";
    private static final String ARG_LOG_FILE_NAME = "LOG_FILE_NAME";
    private static final String ARG_LISTEN_PORT = "LISTEN_PORT";
    private static final String ARG_PKT_NUMBER = "PKT_NUMBER";
    private static final String ARG_PKT_LENGTH = "PKT_LENGTH";
    private static final String ARG_PKT_RATE = "PKT_RATE";
    private static final String ARG_NET_HOPS = "NET_HOPS";
    private static final String ARG_TO_STOP = "APP_STOP";
    private static final String ARG_DELAY = "DELAY";

    //intent wifi arguments
    private static final String ARG_SSID = "SSID";
    private static final String ARG_PASS = "PASS";
    private static final String ARG_TO_REMOVE = "TO_REMOVE";

    // intent enable formation arguments
    public static final String ARG_DIS_LM_LW = "DIS_LM_LW"; // min discovery time. INTEGER
    public static final String ARG_DIS_LM_HG = "DIS_LM_HG"; // max discovery time. INTEGER
    public static final String ARG_VIS_LM_LW = "VIS_LM_LW"; // min visibility time. INTEGER
    public static final String ARG_VIS_LM_HG = "VIS_LM_HG"; // max visibility time. INTEGER
    public static final String ARG_CONN_LM_LW = "CONN_LM_LW"; // min connections number. INTEGER
    public static final String ARG_CONN_LM_HG = "CONN_LM_HG"; // max connections number. INTEGER
    public static final String ARG_ACCEPT_PROB = "ACCEPT_PROB"; // accept connections probability. FLOAT
    public static final String ARG_IDLE_TIME = "IDLE_TIME"; // master idle time. INTEGER
    public static final String ARG_ENABLE_HW = "ENABLE_HW"; // to enable hardware at startup. BOOLEAN

    public static final String[] FORM_ARGS = {
            ARG_DIS_LM_LW, ARG_DIS_LM_HG,
            ARG_VIS_LM_LW, ARG_VIS_LM_HG,
            ARG_CONN_LM_LW, ARG_CONN_LM_HG,
            ARG_ACCEPT_PROB,
            ARG_IDLE_TIME,
            ARG_ENABLE_HW
    };


    /**
     * Intents action definitions
     */
    public enum Command {
        NETWORK_START("android.hyrax.network_start"),
        NETWORK_STOP("android.hyrax.network_stop"),
        FORMATION_ENABLE("android.hyrax.formation_enable"),
        FORMATION_DISABLE("android.hyrax.formation_disable"),
        FORMATION_PAUSE("android.hyrax.formation_pause"),
        FORMATION_RESUME("android.hyrax.formation_resume"),
        SEND_PACKETS("android.hyrax.send_packets"),

        WIFI_HW_ENABLE("android.hyrax.wifi_hw_enable"),
        WIFI_HW_DISABLE("android.hyrax.wifi_hw_disable"),
        WIFI_NET_CONNECT("android.hyrax.wifi_net_connect"),
        WIFI_NET_DISCONNECT("android.hyrax.wifi_net_disconnect");
        private String ref;

        /**
         * Constructor.
         *
         * @param ref string identifier
         */
        Command(@NonNull String ref) {
            this.ref = ref;
        }

        /**
         * Returns an Command object given a string.
         *
         * @param ref string reference
         * @return Command object or null of no action found
         */
        @Nullable
        public static Command toCommand(@NonNull String ref) {
            for (Command action : Command.values())
                if (action.ref.equals(ref))
                    return action;
            return null;
        }
    }

    @Nullable
    private WifiSetup wifiSetup;
    private final AdbBroadcastListener listener;
    public final PropertiesManager propertiesManager;

    /**
     * Constructor.
     *
     * @param listener adb listener object
     */
    public AdbBroadcast(@NonNull AdbBroadcastListener listener) {
        this.listener = listener;
        PropertiesManager.clean();
        this.propertiesManager = new PropertiesManager();
    }

    /**
     * Enables the wifi setup service.
     * Listens for wifi commands enable, disable, connect ...
     *
     * @param context application context
     */
    public void enableWifiSetup(@NonNull Context context) {
        this.wifiSetup = new WifiSetup(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Objects.requireNonNull(intent.getAction());
        final Command cmd = Command.toCommand(intent.getAction());
        if (cmd == null)
            throw new RuntimeException("Undefined command " + intent.getAction());
        switch (cmd) {
            case NETWORK_START:
                listener.onNetworkStart(getArgString(intent, ARG_TYPE),
                        getArgString(intent, ARG_LOG_FILE_NAME),
                        Integer.valueOf(getArgString(intent, ARG_LISTEN_PORT, "0"))
                );
                break;
            case NETWORK_STOP:
                listener.onNetworkStop(
                        Boolean.valueOf(getArgString(intent, ARG_TO_STOP, "false")),
                        Long.valueOf(getArgString(intent, ARG_DELAY, "0"))
                );
                break;
            case FORMATION_ENABLE:
                listener.onFormationEnable(getArgString(intent, ARG_TYPE), getMiscArgs(intent, FORM_ARGS));
                break;
            case FORMATION_DISABLE:
                listener.onFormationDisable(getArgString(intent, ARG_TYPE));
                break;
            case FORMATION_PAUSE:
                listener.onFormationPause();
                break;
            case FORMATION_RESUME:
                listener.onFormationResume();
                break;
            case SEND_PACKETS:
                listener.onSendPacket(
                        Integer.valueOf(getArgString(intent, ARG_PKT_NUMBER)),
                        Integer.valueOf(getArgString(intent, ARG_PKT_LENGTH)),
                        Integer.valueOf(getArgString(intent, ARG_PKT_RATE)),
                        Integer.valueOf(getArgString(intent, ARG_NET_HOPS))
                );
                break;
            case WIFI_HW_ENABLE:
                if (wifiSetup != null)
                    wifiSetup.enable();
                break;
            case WIFI_HW_DISABLE:
                if (wifiSetup != null)
                    wifiSetup.disable();
                break;
            case WIFI_NET_CONNECT:
                if (wifiSetup != null) {
                    String ssid = getArgString(intent, ARG_SSID);
                    String pass = getArgString(intent, ARG_PASS, "None");
                    if ("None".equals(pass))
                        wifiSetup.connect(ssid);
                    else
                        wifiSetup.connect(ssid, pass);
                }

                break;
            case WIFI_NET_DISCONNECT:
                if (wifiSetup != null) {
                    wifiSetup.disconnect(
                            getArgString(intent, ARG_SSID),
                            Boolean.valueOf(getArgString(intent, ARG_TO_REMOVE, "false"))
                    );
                }
                break;
            default:
                throw new RuntimeException("Undefined action " + cmd);
        }
    }

    /**
     * Extracts and return a string from intent.
     *
     * @param intent intent object
     * @param key    key to retrieve
     * @return string value
     */
    @NonNull
    private String getArgString(@NonNull Intent intent, @NonNull String key) {
        String arg = intent.getStringExtra(key);
        Objects.requireNonNull(arg);
        return arg;
    }


    /**
     * Extracts and return a string from intent. If not exists return the default value.
     *
     * @param intent       intent object
     * @param key          key to retrieve
     * @param defaultValue default value to return in case the key does not exists
     * @return string value
     */
    @NonNull
    private String getArgString(@NonNull Intent intent, @NonNull String key, @NonNull String defaultValue) {
        String arg = intent.getStringExtra(key);
        return (arg == null) ? defaultValue : arg;
    }

    @NonNull
    private Map<String, String> getMiscArgs(@NonNull Intent intent, @NonNull String[] args) {
        Map<String, String> map = new HashMap<>();
        for (String key : args) {
            String argVal = intent.getStringExtra(key);
            if (argVal != null)
                map.put(key, argVal);
        }
        return map;
    }

    /**
     * Returns the intent filters allowed.
     *
     * @return intent filter object
     */
    public IntentFilter getIntentFilters() {
        IntentFilter filter = new IntentFilter();
        for (Command cmd : Command.values())
            filter.addAction(cmd.ref);
        return filter;
    }

    /**
     * File properties manager.
     */
    public static class PropertiesManager {
        private final FileOutputStream outputStream;

        /**
         * Constructor
         */
        private PropertiesManager() {
            //this.outputStream = new FileOutputStream(BARRIER_FILE, true);
            try {
                this.outputStream = new FileOutputStream(BARRIER_FILE);//no append
            } catch (FileNotFoundException e) {
                throw new RuntimeException("Error open barrier file");
            }
        }

        /*public synchronized static void reset() {
            propertiesManager.clean(true);
        }*/

        /**
         * Changes a list of properties.
         *
         * @param pairs properties (key, value) paris to be set
         */
        public synchronized void setProperties(String... pairs) {
            if (pairs.length % 2 != 0)
                throw new RuntimeException("The argument arity must be pair");

            Properties properties = new Properties();
            for (int i = 0; i < pairs.length; i += 2)
                properties.setProperty(pairs[i], pairs[i + 1]);
            store(properties);
        }

        /**
         * Stores the object properties on file.
         *
         * @param properties object properties
         */
        private void store(@NonNull Properties properties) {
            try {
                properties.store(outputStream, null);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Error storing properties values");
            }
        }

        /**
         * Closes the file output stream.
         */
        public void close() {
            try {
                outputStream.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }

        /**
         * Cleans the properties file.
         */
        private static void clean() {
            try (PrintWriter writer = new PrintWriter(BARRIER_FILE)) {
                writer.print("");
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
