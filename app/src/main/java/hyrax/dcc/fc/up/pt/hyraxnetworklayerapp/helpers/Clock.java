/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package hyrax.dcc.fc.up.pt.hyraxnetworklayerapp.helpers;


public final class Clock {

    public static final long NANOS_PER_MILLIS = 1_000_000L;
    public static final long NANOS_PER_SECOND = 1_000_000_000L;
    public static final double NANOS_TO_SEC_FACTOR = 1e-09;
    public static final String DEFAULT_NTP_ADDRESS = "1.pt.pool.ntp.org";
    private static final Clock INSTANCE = new Clock();
    private static final int NANO_ADJ_SAMPLES = 100;
    private final long wallTimeRef;
    private final long epochOffset;
    private long ntpDelta;


    private Clock() {
        long d = 0;
        for (int i = 0; i < NANO_ADJ_SAMPLES; i++) {
            d += System.currentTimeMillis() - System.nanoTime() / NANOS_PER_MILLIS;
        }
        epochOffset = NANOS_PER_MILLIS * (d / NANO_ADJ_SAMPLES);
        wallTimeRef = epochOffset + System.nanoTime();
        ntpDelta = 0L;
    }

    public synchronized static void synchronizeWithNTPServer(String address) {
        INSTANCE.ntpDelta = NTPSync.getDelta(address) * NANOS_PER_MILLIS;
    }

    public static double now() {
        return epochTimeNano() * NANOS_TO_SEC_FACTOR;
    }

    public static double wallTime() {
        return (epochTimeNano() - INSTANCE.wallTimeRef) * NANOS_TO_SEC_FACTOR;
    }

    public static long epochTimeNano() {
        return System.nanoTime() + INSTANCE.epochOffset + INSTANCE.ntpDelta;
    }

    public static long epochTimeMillis() {
        return epochTimeNano() / NANOS_PER_MILLIS;
    }

    public static long getNtpDelta() {
        return INSTANCE.ntpDelta;
    }
}
